const bodyParser = require('body-parser')
const session = require('express-session')

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'YSS CRM CLOUD',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'ระบบบริหารจัดการงานขาย' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'style/sheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.42/css/uikit.min.css' }
    ],
    script: [
      {src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.42/js/uikit.min.js', type: 'text/javascript'},
      {src: 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.42/js/uikit-icons.min.js', type: 'text/javascript'}
    ]
  },
  modules: [
    ['@nuxtjs/pwa', { icon: false }]
  ],
  /*
  ** Global CSS
  */
  css: [
    'bulma',
    // 'iview/dist/styles/iview.css',
    'vuesax/dist/vuesax.css',
    'vue-flatpickr/theme/dark.css',
    'font-awesome/scss/font-awesome.scss',
    'font-awesome-animation/dist/font-awesome-animation.css',
    // 'vue-images/dist/vue-images.css',
    '@/assets/css/hero.css',
    '@/assets/css/main.css',
    '@/assets/scss/express.scss'
  ],
  /*
  ** Add axios globally
  */
  build: {
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    },
    vendor: [
      '~/plugins/axios',
      '~/plugins/vuesax',
      '~/plugins/vue-lazyload',
      '~/plugins/vue-flatpickr',
      '~/plugins/vue2-filters',
      '~/plugins/vue-toasted',
      '~/plugins/vue-infinite-loading',
      // '~/plugins/firebase',
      '~/plugins/vue-progressive-image',
      '~/plugins/vlightbox'
    ],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        }, {
          test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/
        })
      }
    }
  },
  serverMiddleware: [
    // body-parser middleware
    bodyParser.json(),
    // session middleware
    session({
      secret: 'super-secret-key',
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 60000 }
    })
    // ,
    // Api middleware
    // We add /api/login & /api/logout routes
    // '~/server'
  ]
}
