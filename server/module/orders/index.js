import ENV from '../../config'
import Greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
var guid = require('../../../assets/js/greeter')
const r = require('rethinkdb')
// var mysql = require('mysql')
var GetOrdersAxOrderSetDateTime = (empid) => {
  return new Promise(resolve => {
    MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
      assert.equal(err, null)
      db.collection('axorder')
        .find({purchorderformnum: empid})
        .toArray((err, rs) => {
          assert.equal(err, null)
          var i = 0
          var loopArray = (j, callback) => {
            // console.log(rs[j].salesid)
            db.collection('axorder').update({salesid: rs[j].salesid}, {$set: {created: new Date(rs[j].createddatetime)}}, (ee, rg) => {
              assert.equal(ee, null)
              callback()
            })
          }
          var Arr = (data) => {
            loopArray(i, () => {
              i++
              if (i < data.length) {
                Arr(data)
              } else {
                resolve()
              }
            })
          }
          Arr(rs)
        })
    })
  })
}
export default {
  GetOrders: (territory,position,empcode,token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey)
            }
            var ter = rkey.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            console.dir(b)
            let ordertype = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            if (position <= 8) {
              ordertype = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
            }
            db.collection('salesorder').find({
              salesdistrictid: {
                $in: b
              },
              ordertype: {
                $in: ordertype
              }
            }).sort({
              updateat: -1,
              approve: 1
            })
              .limit(100)
              .toArray((err, rs) => {
                assert.equal(err, null)
                resolve(rs)
              })
          })
      })
    })
  },
  GetOrderListByCustomer: (empid, custname, position) => {
    return new Promise(resolve => {
      // const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        let ordertype = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        // if (position <= 8) {
        //   ordertype = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        // }
        // var n = new Date()
        // var s = n.getFullYear() + '-' + ('0' + n.getMonth()).slice(-2) + '-01T00:00:00Z'
        // var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + (n.getDate() + 1)).slice(-2) + 'T00:00:00Z'
        if (custname !== null) {
          db.collection('salesorder')
            .aggregate([
              {
                $match: {
                  filtername: {
                    $regex: custname
                  }
                }
              },
              {
                $sort: {
                  updateat: -1,
                  approve: 1
                }
              }
            ])
            .toArray((err, rs) => {
              assert.equal(err, null)
              resolve(rs)
            })
        } else {
          db.collection('salesorder')
            .aggregate([
              {
                $match: {
                  ordertype: {
                    $in: ordertype
                  },
                  createby: empid
                }
              },
              {
                $sort: {
                  updateat: -1,
                  approve: 1
                }
              },
              {
                $limit: 50
              }
            ])
            .toArray((err, rs) => {
              assert.equal(err, null)
              resolve(rs)
            })
        }
      })
    })
  },
  GetOrderListById: (empid, position) => {
    return new Promise(resolve => {
      // const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        let ordertype = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        // if (position <= 8) {
        //   ordertype = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        // }
        db.collection('salesorder')
          .aggregate([
            { $match: {
              ordertype: {
                $in: ordertype
              },
              createby: empid
            }
            }, {
              $sort: {
                updateat: -1,
                approve: 1
              }
            }
          ])
          .toArray((err, rs) => {
            assert.equal(err, null)
            resolve(rs)
          })
      })
    })
  },
  GetOrdersAxOrder: (territory, empid, position, customer) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        console.log('data:', territory, empid, position, customer)
        GetOrdersAxOrderSetDateTime(empid)
          .then(rg => {
            var n = new Date()
            // var s = n.getFullYear() + '-' + ('0' + n.getMonth()).slice(-2) + '-01T00:00:00Z'
            // var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + (n.getDate() + 1)).slice(-2) + 'T00:00:00Z'
            if (customer !== null) {
              db.collection('axorder')
                .aggregate([
                  {
                    $match: {
                      filtername: {$regex: customer}
                    }
                  }
                ])
                .toArray((err, rs) => {
                  assert.equal(err, null)
                  resolve(rs)
                })
            } else {
              var ss = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + (n.getDate() - 3)).slice(-2) + 'T00:00:00Z'
              var ee = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + (n.getDate() + 1)).slice(-2) + 'T00:00:00Z'
              db.collection('axorder')
                .aggregate([
                  {
                    $match: {
                      'created': {
                        '$gte': new Date(ss.toString()),
                        '$lte': new Date(ee.toString())
                      },
                      purchorderformnum: {
                        $regex: empid
                      }
                    }
                  },
                  {
                    $limit: 15
                  }
                ])
                .toArray((err, rs) => {
                  assert.equal(err, null)
                  resolve(rs)
                })
            }
          })
      })
    })
  },
  UpdateOrder: (approveby, orderid, status, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var order = []
        var orderline = []
        db.collection('salesorder').find({ ordernumber: orderid })
          .toArray((err, rs) => {
            assert.equal(err, null)
            order = rs
            orderline = rs[0].orderline
            var cust = order[0].customer
            var n = new Date()
            const orderncode = 'ORD' + cust.substring(0, cust.indexOf('-')) + n.getDate() + n.getMonth() + n.getFullYear() + n.getHours() + n.getMilliseconds()
            var amount = 0
            for (let i in orderline) {
              amount = amount + ((orderline[i].price - ((orderline[i].price * orderline[i].perc) / 100)) * orderline[i].qty)
            }
            db.collection('salesorder').update({ ordernumber: orderid },
              {
                $set: {
                  approveby: approveby,
                  approve: parseInt(status),
                  ordersync: parseInt(1),
                  orderncode: orderncode,
                  salesid: orderncode,
                  salesstatus: parseInt(0)
                }
              }, (err, result) => {
                if (err) {
                  resolve({
                    error: true
                  })
                } else {
                  resolve({
                    error: false
                  })
                }
              })
          })
      })
    })
  },
  InsertOrder: (order, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var cust = order[0].customer
        order[0].created_at = new Date()
        order[0].created = new Date()
        order[0].createat = new Date()
        order[0].updateat = new Date()
        var orline = order[0].orderline
        for (let h in orline) {
          order[0].orderline[h].salesname = cust.substring((cust.indexOf('-') + 1), cust.length)
          order[0].orderline[h].customernumber = cust.substring(0, cust.indexOf('-'))
          order[0].orderline[h].linenumber = h
          order[0].orderline[h].amount = (order[0].orderline[h].price - ((order[0].orderline[h].price * order[0].orderline[h].perc) / 100)) * order[0].orderline[h].qty
          order[0].orderline[h].salesstatus = parseInt(0)
        }
        db.collection('salesorder').insert(order, (err, result) => {
         if (err) {
          resolve({
            error: true,
            result: []
          })
         } else {
          resolve({
            error: false,
            result: result
          })
         }
        })
      })
    })
  }
}
