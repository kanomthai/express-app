import ENV from '../../config'
import Greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
export default {
  GetViewTarget: token => {
    return new Promise(resolve => {
      // const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey)
            }
            var ter = rkey.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            var n = new Date()
            var y = n.getFullYear()
            var m = (n.getMonth() + 1)
            db.collection('yss_targetsalesamtgroupmonth')
              .aggregate([
                {
                  $match: {
                    salesdistrictid: {
                      $in: b
                    },
                    year: y,
                    months: m
                  }
                }
              ])
              .toArray((err, rs) => {
                if (err) throw err
                resolve(rs)
              })
          })
      })
    })
  },
  LoadResultAppointmentActivities: empcode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var n = new Date()
        var s = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-01T00:00:00Z'
        var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-31T00:00:00Z'
        db.collection('resultappointment').aggregate([
          {
            $match: {
              'created': {
                '$gte': new Date(s.toString()),
                '$lte': new Date(e.toString())
              },
              empcode: empcode,
              visitactivities: 1
            }
          },
          {
            $lookup: {
              from: 'custtable',
              localField: 'custnumber',
              foreignField: 'accountnum',
              as: 'custtable'
            }
          }
        ])
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('resultappointment').aggregate([
              {
                $match: {
                  'created': {
                    '$gte': new Date(s.toString()),
                    '$lte': new Date(e.toString())
                  },
                  empcode: empcode,
                  visitactivities: 1
                }
              },
              {
                $group: {
                  _id: '$coutdate'
                }
              }
            ])
              .toArray((err, rescount) => {
                assert.equal(err, null)
                resolve({
                  result: result,
                  rescount: rescount
                })
              })
          })
      })
    })
  },
  LoadResultAppointmentActivitiesMarket: empcode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var n = new Date()
        var s = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-01T00:00:00Z'
        var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-31T00:00:00Z'
        db.collection('resultappointment').aggregate([
          {
            $match: {
              'created': {
                '$gte': new Date(s.toString()),
                '$lte': new Date(e.toString())
              },
              empcode: empcode,
              visitconditions: 1
            }
          },
          {
            $lookup: {
              from: 'custtable',
              localField: 'custnumber',
              foreignField: 'accountnum',
              as: 'custtable'
            }
          }
        ])
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('resultappointment').aggregate([
              {
                $match: {
                  'created': {
                    '$gte': new Date(s.toString()),
                    '$lte': new Date(e.toString())
                  },
                  empcode: empcode,
                  visitconditions: 1
                }
              },
              {
                $group: {
                  _id: '$coutdate'
                }
              }
            ])
              .toArray((err, rescount) => {
                assert.equal(err, null)
                resolve({
                  result: result,
                  rescount: rescount
                })
              })
          })
      })
    })
  }
}
