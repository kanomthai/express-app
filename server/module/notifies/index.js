import ENV from '../../config'
var request = require('request')
export default {
  LineNottifiesSticker: (message, sid, snum, imageThumbnail) => {
    return new Promise(resolve => {
      request({
        method: 'POST',
        uri: 'https://notify-api.line.me/api/notify',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        auth: {
          'bearer': ENV.LineToken
        },
        form: {
          message: message,
          stickerPackageId: sid,
          stickerId: snum
        }
      }, (err, httpResponse, body) => {
        if (err) {
          resolve(err)
        } else {
          resolve({
            httpResponse: httpResponse,
            body: body
          })
        }
      })
    })
  },
  LineNottifies: message => {
    return new Promise(resolve => {
      request({
        method: 'POST',
        uri: 'https://notify-api.line.me/api/notify',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        auth: {
          'bearer': ENV.LineToken
        },
        form: {
          message: message
        }
      }, (err, httpResponse, body) => {
        if (err) {
          resolve(err)
        } else {
          resolve({
            httpResponse: httpResponse,
            body: body
          })
        }
      })
    })
  }
}
