import ENV from '../../config'
var Connection = require('tedious').Connection
var Request = require('tedious').Request
var config = {
  userName: ENV.MSSQL_USERS,
  password: ENV.MSSQL_PWD,
  server: ENV.MSSQL_HOST,
  options: {
    database: ENV.MSSQL_DB_NAME
  }
}
export default {
  GetCustCredit: () => {
    return new Promise(resolve => {
      var connection = new Connection(config)
      connection.on('connect', function (err) {
        if (err) {
          console.log(err)
        } else {
          executeStatement()
        }
      })
      function executeStatement () {
        const request = new Request(`select account from cloudcustdisc`, (err, rowCount) => {
          if (err) {
            console.log(err)
          }
          connection.close()
        })
        request.on('row', columns => {
          let b = []
          resolve(columns)
          if (columns.length < 1) {
            resolve(b)
          } else {
            b.length = 0
            var rowObject = {}
            columns.forEach(column => {
              rowObject[column.metadata.colName] = column.value
            })
            b.push(rowObject)
            resolve(b)
          }
        })
        connection.execSql(request)
      }
    })
  }
}
