import ENV from '../../config'
import Greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var guid = require('../../../assets/js/greeter')
var assert = require('assert')
var n = new Date()
// var d = 25
var m = n.getMonth()
var s = n.getFullYear() + '-' + ('0' + (m) + 1).slice(-2) + '-25T00:00:00Z'
var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 2)).slice(-2) + '-' + ('0' + (n.getDate() + 2)).slice(-2) + 'T12:00:00Z'
export default {
  GetCustAppointmentsListApprove: (empcode, territory, ownerthor) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var q = {
          territory: {
            $in: territory
          },
          status: 1
        }
        db.collection('appointment')
          .find(q)
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  updateCustAppointmentsApproveTerritory: (territory, status, remark) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        // var n = new Date()
        db.collection('appointment')
          .updateMany({status: 1, territory: territory}, {$set: {
            status: status,
            syncstatus: 0,
            remark: remark,
            approve: (status - 1),
            updated_at: new Date()
          }}, (er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  updateCustAppointmentsApprove: (appointcode, status, territory) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        // var n = new Date()
        db.collection('appointment')
          .find({status: 1})
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  AppointsLoadCustomer: (token, empcode, txtfilter) => {
    return new Promise(resolve => {
      // const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey)
            }
            var ter = rkey.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            if (txtfilter) {
              if (txtfilter.length > 0) {
                db.collection('custtable')
                  .aggregate([
                    {
                      $match: {
                        salesdistrictid: {
                          $in: b
                        },
                        filtername: {
                          $regex: txtfilter
                        }
                      }
                    },
                    {
                      $lookup: {
                        from: 'custaddress',
                        localField: 'partyid',
                        foreignField: 'tableid',
                        as: 'address'
                      }
                    },
                    {
                      $lookup: {
                        from: 'appointment',
                        localField: 'accountnum',
                        foreignField: 'custnumber',
                        as: 'appointment'
                      }
                    },
                    {
                      $sort: {
                        closedaccount: 1,
                        salesdistrictid: 1
                      }
                    }
                  ])
                  .toArray((err, rs) => {
                    assert.equal(err, null)
                    resolve(rs)
                  })
              } else {
                db.collection('custtable')
                  .aggregate([
                    {
                      $match: {
                        salesdistrictid: {
                          $in: b
                        }
                      }
                    },
                    {
                      $lookup: {
                        from: 'custaddress',
                        localField: 'partyid',
                        foreignField: 'tableid',
                        as: 'address'
                      }
                    },
                    {
                      $lookup: {
                        from: 'appointment',
                        localField: 'accountnum',
                        foreignField: 'custnumber',
                        as: 'appointment'
                      }
                    },
                    {
                      $sort: {
                        closedaccount: 1,
                        salesdistrictid: 1
                      }
                    }
                  ])
                  .toArray((err, rs) => {
                    assert.equal(err, null)
                    resolve(rs)
                  })
              }
            } else {
              db.collection('custtable')
                .aggregate([
                  {
                    $match: {
                      salesdistrictid: {
                        $in: b
                      }
                    }
                  },
                  {
                    $lookup: {
                      from: 'custaddress',
                      localField: 'partyid',
                      foreignField: 'tableid',
                      as: 'address'
                    }
                  },
                  {
                    $lookup: {
                      from: 'appointment',
                      localField: 'accountnum',
                      foreignField: 'custnumber',
                      as: 'appointment'
                    }
                  },
                  {
                    $sort: {
                      closedaccount: 1,
                      salesdistrictid: 1
                    }
                  }
                ])
                .toArray((err, rs) => {
                  assert.equal(err, null)
                  resolve(rs)
                })
            }
          })
      })
    })
  },
  GetCustAppointments: (empcode, territory) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var n = new Date()
        db.collection('appointment')
          .find({empcode: empcode, territory: territory, appointmentmonths: n.getMonth(), appointmentyear: n.getFullYear()})
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  AppointsCustomerAdd: (territory, empcode, approve, Obj) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        console.log(Obj.schedule)
        db.collection('appointment')
          .find({appointmentcode: Obj.appointmentcode})
          .toArray((er, res) => {
            assert.equal(err, null)
            Obj.updated = new Date()
            Obj.approve = approve
            Obj.syncstatus = 0
            var q = {appointmentcode: Obj.appointmentcode}
            if (res.length > 0) {
              db.collection('appointment').update(q, {$set: Obj}, (err, ty) => {
                assert.equal(err, null)
                db.collection('appointment')
                  .find(q)
                  .toArray((err, ress) => {
                    assert.equal(err, null)
                    resolve(ress)
                  })
              })
            } else {
              Obj.created = new Date()
              Obj.approve = approve
              Obj.syncstatus = 0
              Obj.appointid = guid.GenCode()
              db.collection('appointment').insert(Obj, (err, ty) => {
                assert.equal(err, null)
                db.collection('appointment')
                  .find(q)
                  .toArray((err, ress) => {
                    assert.equal(err, null)
                    resolve(ress)
                  })
              })
            }
          })
      })
    })
  },
  InsAppointments: (empcode, territory, Obj) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        console.log(Obj.schedule)
        db.collection('appointment')
          .find({appointmentcode: Obj.appointmentcode})
          .toArray((er, res) => {
            assert.equal(err, null)
            // var n = new Date()
            Obj.updated = new Date()
            var q = {appointmentcode: Obj.appointmentcode}
            if (res.length > 0) {
              db.collection('appointment').update(q, {$set: Obj}, (err, ty) => {
                assert.equal(err, null)
                db.collection('appointment')
                  .find(q)
                  .toArray((err, ress) => {
                    assert.equal(err, null)
                    resolve(ress)
                  })
              })
            } else {
              Obj.created = new Date()
              db.collection('appointment').insert(Obj, (err, ty) => {
                assert.equal(err, null)
                db.collection('appointment')
                  .find(q)
                  .toArray((err, ress) => {
                    assert.equal(err, null)
                    resolve(ress)
                  })
              })
            }
          })
      })
    })
  },
  AppointsCheckStatus: appointmentcode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .find({appointmentcode: appointmentcode})
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  GetRegCusttableTerritoryAppointments: (territory, empcode) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .find({
            territory: territory,
            empcode: empcode,
            created: {
              $gte: new Date(s),
              $lte: new Date(e)
            }
          })
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  AppointsLoadNew: territory => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        // let n = new Date()
        // let xm = n.getMonth() + 2
        // let xy = n.getFullYear()
        // if (xm > 12) {
        //   xm = 1
        //   xy = n.getFullYear() + 1
        // }
        // let schedate = '01-' + ('0' + (xm)).slice(-2) + '-' + xy
        db.collection('appointment')
          .find({
            territory: territory,
            created: {
              $gte: new Date(s),
              $lte: new Date(e)
            }
          })
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  AppointsLoad: territory => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .find({
            territory: territory,
            created: {
              $gte: new Date(s),
              $lte: new Date(e)
            }
          })
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  },
  GroupCountAppointments: territory => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var n = new Date()
        // var d = 25
        var m = n.getMonth()
        var s = n.getFullYear() + '-' + ('0' + (m + 1)).slice(-2) + '-20T00:00:00Z'
        db.collection('appointment')
          .aggregate([
            {
              $match: {
                empteritory: territory,
                updated: {
                  $gte: new Date(s)
                }
              }
            }, {$group: { _id: '$schedule' }} ])
          .toArray((er, res) => {
            assert.equal(err, null)
            db.collection('appointment')
              .aggregate([ {$match: {
                empteritory: territory,
                updated: {
                  $gte: new Date(s)
                }
              }
              }, {$group: { _id: '$_id' }} ])
              .toArray((er, red) => {
                assert.equal(err, null)
                resolve({
                  count: res,
                  total: red
                })
              })
          })
      })
    })
  },
  GroupAppointments: territory => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .aggregate([ {$match: {
            territory: territory,
            created: {
              $gte: new Date(s),
              $lte: new Date(e)
            }
          }
          }, {$group: { _id: '$schedule' }} ])
          .toArray((er, res) => {
            assert.equal(err, null)
            db.collection('appointment')
              .aggregate([ {$match: {
                territory: territory,
                created: {
                  $gte: new Date(s),
                  $lte: new Date(e)
                }
              }
              }, {$group: { _id: '$_id' }} ])
              .toArray((er, red) => {
                assert.equal(err, null)
                resolve({
                  count: res,
                  total: red
                })
              })
          })
      })
    })
  },
  AppointsApprove: (appointmentcode, status, remark) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .update({appointmentcode: appointmentcode}, {
            $set: {
              status: status,
              syncstatus: 0,
              remark: remark,
              approve: (status - 1),
              updated: new Date()
            }
          }, (err, re) => {
            assert.equal(err, null)
            resolve()
          })
      })
    })
  },
  AppointsCopy: (appointmentcode, territory, empcode) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .find({appointmentcode: appointmentcode})
          .toArray((err, result) => {
            assert.equal(err, null)
            var g = guid.GenCode()
            var n = new Date()
            var b = {
              appointid: g,
              appointmentcode: result[0].appointmentcode + n.getMilliseconds(),
              custnumber: result[0].accountnum,
              custname: result[0].name,
              accountnum: result[0].accountnum,
              name: result[0].name,
              billing: result[0].billing,
              billingamount: result[0].billingamount,
              schedule: result[0].schedule,
              filtername: result[0].filtername,
              activities: result[0].activities,
              territory: territory,
              empcode: empcode,
              remark: result[0].remark,
              status: 1,
              created: new Date(),
              updated: new Date()
            }
            db.collection('appointment').insert(b, (err, re) => {
              assert.equal(err, null)
              resolve()
            })
          })
      })
    })
  },
  GetAppointsApprove: (territory, status) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('appointment')
          .find({territory: territory,
            created: {
              $gte: new Date(s),
              $lte: new Date(e)
            }
          })
          .toArray((err, re) => {
            assert.equal(err, null)
            resolve(re)
          })
      })
    })
  }
}
