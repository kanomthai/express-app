import ENV from '../../config'
// var c = require('colors')
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
var mysql = require('mysql')
var n = new Date()
var q = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + n.getDate()).slice(-2)

export default {
  GetBackOrder: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from cloudbackorder where updated_at >= '` + q + `'`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            data[i].updated_at = new Date()
            var ins = data[i]
            db.collection('custtablebackorder')
              .find({recid: e.recid})
              .toArray((egr, rs) => {
                if (rs.length > 0) {
                  db.collection('custtablebackorder').update({recid: e.recid}, {$set: ins})
                } else {
                  db.collection('custtablebackorder').insert(ins)
                }
              })
          })
          console.log('Sync Backorder completed!')
          connection.end()
          resolve({
            success: true,
            message: 'บันทึกข้อมูลเรียบร้อยแล้ว'
          })
        })
      })
    })
  },
  GetCusttable: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      // select * from custtable where updated_at >= '` + q + `'
      connection.query(`select * from custtable`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            data[i].updated_at = new Date()
            var ins = data[i]
            db.collection('custtable').find({accountnum: e.accountnum})
              .toArray((egr, rg) => {
                if (egr) throw egr
                if (rg.length > 0) {
                  db.collection('custtable').update({accountnum: e.accountnum}, {$set: ins})
                } else {
                  db.collection('custtable').insert(ins)
                }
              })
          })
        })
        connection.end()
        resolve({
          success: true,
          message: 'บันทึกข้อมูลเสร็จแล้ว'
        })
      })
    })
  },
  GetCusttrans: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from custtrans where updated_at >= '` + q + `'`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            db.collection('custtrans').find({accountnum: e.accountnum}).toArray((ee, rs) => {
              if (ee) throw ee
              if (rs.length > 0) {
                db.collection('custtrans').update({accountnum: e.accountnum}, {$set: data[i]})
              } else {
                db.collection('custtrans').insert(data[i])
              }
            })
          })
        })
      })
      connection.end()
      resolve({
        success: true,
        message: 'บันทึกข้อมูลเสร็จแล้ว'
      })
    })
  },
  GetCustAddress: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from address where updated_at >= '` + q + `'`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            db.collection('custaddress')
              .find({recid: e.recid})
              .toArray((egr, rs) => {
                if (rs.length > 0) {
                  db.collection('custaddress').update({recid: e.recid}, {$set: data[i]})
                } else {
                  db.collection('custaddress').insert(data[i])
                }
              })
          })
        })
      })
      connection.end()
      resolve({
        success: true,
        message: 'บันทึกข้อมูลเสร็จแล้ว'
      })
    })
  },
  GetCustBalanceCredit: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from balancecredit where updated_at >= '` + q + `'`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            data[i].updated_at = new Date()
            var ins = data[i]
            db.collection('balancecredit')
              .find({accountnum: e.accountnum})
              .toArray((egr, rs) => {
                if (rs.length > 0) {
                  db.collection('balancecredit').update({accountnum: e.accountnum}, {$set: ins})
                } else {
                  db.collection('balancecredit').insert(ins)
                }
              })
          })
        })
      })
      connection.end()
      resolve({
        success: true,
        message: 'บันทึกข้อมูลเสร็จแล้ว'
      })
    })
  },
  GetInventtable: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from inventtable where updated_at >= '` + q + `'`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            data[i].updated_at = new Date()
            var ins = data[i]
            db.collection('inventtable')
              .find({recid: e.recid})
              .toArray((egr, rs) => {
                if (rs.length > 0) {
                  db.collection('inventtable').update({recid: e.recid}, {$set: ins})
                } else {
                  db.collection('inventtable').insert(ins)
                }
              })
          })
        })
      })
      connection.end()
      resolve({
        success: true,
        message: 'บันทึกข้อมูลเสร็จแล้ว'
      })
    })
  },
  GetInventtablePrice: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from inventtableprice where updated_at >= '` + q + `'`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            data[i].updated_at = new Date()
            var ins = data[i]
            db.collection('inventtableprice')
              .find({itemid: e.itemid})
              .toArray((egr, rs) => {
                if (rs.length > 0) {
                  db.collection('inventtableprice').update({itemid: e.itemid}, {$set: ins})
                } else {
                  db.collection('inventtableprice').insert(ins)
                }
              })
          })
          // db.collection('inventtableprice').insert(data, (er, rs) => {
          //   db.close()
          //   resolve({
          //     success: true,
          //     message: 'บันทึกข้อมูลเสร็จแล้ว'
          //   })
          // })
        })
      })
      connection.end()
      resolve({
        success: true,
        message: 'บันทึกข้อมูลเสร็จแล้ว'
      })
    })
  },
  GetCustCreditDisc: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query(`select * from custtablecredit`, (err, rows, fields) => {
        if (err) throw err
        var data = rows
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          data.forEach((e, i) => {
            data[i].updated_at = new Date()
            var ins = data[i]
            db.collection('custtablecreditdisc')
              .find({account: e.account})
              .toArray((egr, rs) => {
                if (rs.length > 0) {
                  db.collection('custtablecreditdisc').update({account: e.account}, {$set: ins})
                } else {
                  db.collection('custtablecreditdisc').insert(ins)
                }
              })
          })
        })
      })
      connection.end()
      resolve({
        success: true,
        message: 'บันทึกข้อมูลเสร็จแล้ว'
      })
    })
  }
}
