import ENV from '../../config'
import Greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
const r = require('rethinkdb')
var guid = require('../../../assets/js/greeter')

export default {
  InsertPostPect: (postpect, token) => {
    return new Promise(resolve => {
      const appkey = guid.GenCode()
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey.err)
            }
            Greeter.CheckToken(token)
              .then(ykey => {
                console.dir(ykey)
                let n = new Date()
                db.collection('commercialcustomers').find()
                  .toArray((e, r) => {
                    postpect.createby = ykey.emp.key.empcode
                    postpect.create = new Date()
                    postpect.postpectnumber = postnum
                    db.collection('commercialcustomers').insert(postpect, (err, rs) => {
                      if (err) throw err
                      var appoint = [{
                        resultcode: appkey,
                        empcode: ykey.emp.key.empcode,
                        empname: ykey.emp.key.name,
                        custtomer: postpect.postnum + '-' + postpect.name,
                        custnumber: postpect.postnum,
                        custname: postpect.name,
                        salesdistrictid: postpect.postnum,
                        visit: 1,
                        visittype: 8,
                        visittitle: 'บันทึกลูกค้าค้าดหวัง',
                        visitdate: n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + n.getDate()).slice(-2),
                        visittime: ('0' + n.getHours()).slice(-2) + '-' + ('0' + n.getMinutes()).slice(-2),
                        visitremark: null,
                        visitactivitieslist: null,
                        latitude: null,
                        longitude: null,
                        localtionmsg: null,
                        pathname: postpect.pathname,
                        syncstatus: 0,
                        created: new Date(),
                        updated: new Date()
                      }]
                      db.collection('resultappointment').insert(appoint, (err, rs) => {
                        if (err) throw err
                        r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                          assert.equal(err, null)
                          r.db('cloud').table('resultappointments').insert(appoint).run(conn, (err, res) => {
                            assert.equal(err, null)
                            resolve(appoint)
                          })
                        })
                      })
                    })
                  })
              })
          })
      })
    })
  },
  LoadResultAppointment: (empcode, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey.err)
            }
            var n = new Date()
            var s = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-01T00:00:00Z'
            var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-31T00:00:00Z'
            db.collection('resultappointment').aggregate([
              {
                $match: {
                  'created': {
                    '$gte': new Date(s.toString()),
                    '$lte': new Date(e.toString())
                  },
                  empcode: empcode
                }
              },
              {
                $sort: {
                  created: -1
                }
              }
            ])
              .toArray((err, result) => {
                assert.equal(err, null)
                resolve(result)
              })
          })
      })
    })
  },
  InsertResultAppointmentNewUpdate: (Obj, token) => {
    return new Promise(resolve => {
      const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.GetGPS().then(g => {
          Greeter.CheckTerritory(token)
            .then(rkey => {
              if (rkey.err === true) {
                resolve(rkey.err)
              }
              Greeter.CheckToken(token)
                .then(ykey => {
                  var n = new Date()
                  db.collection('masteractivities')
                    .find({id: Obj.visittype})
                    .toArray((err, req) => {
                      assert.equal(err, null)
                      var xnull = (txt) => {
                        if (txt) {
                          return null
                        } else {
                          return txt
                        }
                      }
                      var u = {
                        resultcode: appkey,
                        empcode: ykey.emp.key.empcode,
                        empname: ykey.emp.key.name,
                        custtomer: xnull(Obj.custtomer),
                        custnumber: xnull(Obj.custnumber),
                        custname: xnull(Obj.custname),
                        salesdistrictid: xnull(Obj.territory),
                        visit: 1,
                        visittype: xnull(Obj.visittype),
                        visittitle: req[0].title,
                        visitdate: n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + n.getDate()).slice(-2),
                        visittime: ('0' + n.getHours()).slice(-2) + ':' + ('0' + n.getMinutes()).slice(-2),
                        visitremark: xnull(Obj.remark),
                        visitactivitieslist: xnull(Obj.activities),
                        billingnumber: Obj.billinglist,
                        collectiondetail: xnull(Obj.collectiondetail),
                        collectiontype: Obj.collectiontype,
                        moneydate: new Date(),
                        moneyamount: xnull(Obj.moneyamount),
                        latitude: xnull(Obj.latitude),
                        longitude: xnull(Obj.longitude),
                        localtionmsg: xnull(Obj.localtionmsg),
                        pathname: xnull(Obj.pathname),
                        syncstatus: 0,
                        created: new Date(),
                        updated: new Date()
                      }
                      db.collection('resultappointment').insert(u, (err, rs) => {
                        assert.equal(err, null)
                        r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                          assert.equal(err, null)
                          r.db('cloud').table('resultappointments').insert(u).run(conn, (err, res) => {
                            assert.equal(err, null)
                            resolve(u)
                          })
                        })
                      })
                    })
                })
            })
        })
      })
    })
  },
  InsertResultAppointment: (Obj, token) => {
    return new Promise(resolve => {
      const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.GetGPS().then(g => {
          Greeter.CheckTerritory(token)
            .then(rkey => {
              if (rkey.err === true) {
                resolve(rkey.err)
              }
              Greeter.CheckToken(token)
                .then(ykey => {
                  var n = new Date()
                  db.collection('masteractivities')
                    .find({id: Obj.visittype})
                    .toArray((err, req) => {
                      assert.equal(err, null)
                      var u = {
                        resultcode: appkey,
                        empcode: ykey.emp.key.empcode,
                        empname: ykey.emp.key.name,
                        custtomer: Obj.custtomer,
                        custnumber: Obj.custnumber,
                        custname: Obj.custname,
                        salesdistrictid: Obj.territory,
                        visit: 1,
                        visittype: Obj.visittype,
                        visittitle: req[0].title,
                        visitdate: n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-' + ('0' + n.getDate()).slice(-2),
                        visittime: ('0' + n.getHours()).slice(-2) + ':' + ('0' + n.getMinutes()).slice(-2),
                        visitremark: Obj.remark,
                        visitactivitieslist: Obj.activities,
                        latitude: Obj.latitude,
                        longitude: Obj.longitude,
                        localtionmsg: Obj.localtionmsg,
                        pathname: Obj.pathname,
                        syncstatus: 0,
                        created: new Date(),
                        updated: new Date()
                      }
                      db.collection('resultappointment').insert(u, (err, rs) => {
                        assert.equal(err, null)
                        r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                          assert.equal(err, null)
                          r.db('cloud').table('resultappointments').insert(u).run(conn, (err, res) => {
                            assert.equal(err, null)
                            resolve(u)
                          })
                        })
                      })
                    })
                })
            })
        })
      })
    })
  },
  // InsertResultAppointment: (data, token) => {
  //   return new Promise(resolve => {
  //     const appkey = guid.GenCode()
  //     var Obj = data[0]
  //     MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
  //       assert.equal(err, null)
  //       Greeter.GetGPS().then(g => {
  //         var gps = [{
  //           ip: g.data.ip,
  //           country_code: g.data.country_code,
  //           country_name: g.data.country_name,
  //           region_code: g.data.region_code,
  //           region_name: g.data.region_name,
  //           city: g.data.city,
  //           zip_code: g.data.zip_code,
  //           time_zone: g.data.time_zon,
  //           latitude: g.data.latitude,
  //           longitude: g.data.longitude,
  //           metro_code: g.data.metro_code
  //         }]
  //         Greeter.CheckTerritory(token)
  //           .then(rkey => {
  //             if (rkey.err === true) {
  //               resolve(rkey.err)
  //             }
  //             Greeter.CheckToken(token)
  //               .then(ykey => {
  //                 var n = new Date()
  //                 const getcheckcode = rkey.territory[0].territorylist[0].signature + Obj.custnumber + n.getDate() + n.getMonth() + n.getFullYear()
  //                 var q = {resultcodecheck: getcheckcode}
  //                 db.collection('resultappointment').find(q)
  //                   .toArray((er, res) => {
  //                     assert.equal(er, null)
  //                     var appoint = [{
  //                       resultcodecheck: getcheckcode,
  //                       resultcode: appkey,
  //                       empcode: ykey.emp.key.empcode,
  //                       empname: ykey.emp.key.name,
  //                       custtomer: Obj.custtomer,
  //                       custnumber: Obj.custnumber,
  //                       custname: Obj.custname,
  //                       transdate: new Date(),
  //                       visittitle: Obj.visittitle,
  //                       visit: Obj.visit,
  //                       visitdate: new Date(Obj.visitdate),
  //                       collectiontitle: Obj.collectiontitle,
  //                       collectiondetail: [],
  //                       visitcollection: Obj.visitcollection,
  //                       visitcollectiondate: new Date(Obj.visitcollectiondate),
  //                       collectiondate: new Date(),
  //                       collectionmoney: 0.0,
  //                       collectiontype: 0,
  //                       visitbillingtitle: Obj.visitbillingtitle,
  //                       visitbilling: Obj.visitbilling,
  //                       visitbillingnumber: null, // Obj.visitbilling,
  //                       visitbillingamount: 0, // Obj.visitbilling,
  //                       visitbillingdate: new Date(Obj.visitbillingdate),
  //                       visitorderstitle: Obj.visitorderstitle,
  //                       visitorders: Obj.visitorders,
  //                       visitordersdate: new Date(Obj.visitordersdate),
  //                       visitactivitiestitle: Obj.visitactivitiestitle,
  //                       visitactivitiestitletxt: null,
  //                       visitactivitieslist: [],
  //                       visitactivities: Obj.visitactivities,
  //                       visitactivitiesdate: new Date(Obj.visitactivitiesdate),
  //                       visitactivitiespostpect: Obj.visitactivitiespostpect,
  //                       visitconditionstitle: Obj.visitconditionstitle,
  //                       visitconditions: Obj.visitconditions,
  //                       visitconditionsdate: new Date(Obj.visitconditionsdate),
  //                       conditionsmkt: null,
  //                       visitcompetitorstitle: Obj.visitcompetitorstitle,
  //                       visitcompetitors: Obj.visitcompetitors,
  //                       visitcompetitorsdate: new Date(Obj.visitcompetitorsdate),
  //                       competitorsdetail: null,
  //                       visitpostpecttitle: Obj.visitpostpecttitle,
  //                       visitpostpect: Obj.visitpostpect,
  //                       visitpostpectdate: new Date(Obj.visitpostpectdate),
  //                       postpectname: null,
  //                       bankname: null,
  //                       banknumber: null,
  //                       latitude: g.data.latitude,
  //                       longtitude: g.data.longitude,
  //                       ipaddress: g.data.ip,
  //                       salesdistrictid: rkey.territory[0].territorylist[0].signature,
  //                       syncstatus: 0,
  //                       moneydate: new Date(),
  //                       moneyamount: 0,
  //                       create: new Date(),
  //                       update: new Date(),
  //                       created: new Date(),
  //                       createdday: n.getDate(),
  //                       createdmonth: n.getMonth() + 1,
  //                       createdyear: n.getFullYear(),
  //                       g: gps
  //                     }]
  //                     if (res.length > 0) {
  //                       var u = {
  //                         empcode: ykey.emp.key.empcode,
  //                         empname: ykey.emp.key.name,
  //                         custtomer: Obj.custtomer,
  //                         custnumber: Obj.custnumber,
  //                         custname: Obj.custname,
  //                         createdday: n.getDate(),
  //                         createdmonth: n.getMonth() + 1,
  //                         createdyear: n.getFullYear(),
  //                         update: new Date()
  //                       }
  //                       if (Obj.visittype === 0) {
  //                         u.visittitle = 'เข้าพบ'
  //                         u.visit = 1
  //                         u.visitdate = new Date()
  //                       } else if (Obj.visittype === 1) {
  //                         u.collectiondetail = Obj.collectiondetail
  //                         u.collectiontitle = 'เก็บเงิน'
  //                         u.visitcollection = 1
  //                         u.visitcollectiondate = new Date()
  //                         u.moneydate = new Date(Obj.moneydate)
  //                         u.moneyamount = Obj.moneyamount
  //                       } else if (Obj.visittype === 2) {
  //                         u.visitbillingtitle = 'วางบิล'
  //                         u.visitbilling = 1
  //                         u.visitbillingdate = new Date()
  //                       } else if (Obj.visittype === 3) {
  //                         u.visitorderstitle = 'เปิดออร์เดอร์'
  //                         u.visitorders = 1
  //                         u.visitordersdate = new Date()
  //                       } else if (Obj.visittype === 4) {
  //                         u.visitactivitieslist = Obj.visitactivitieslist
  //                         u.visitactivitiestitle = 'กิจกรรม'
  //                         u.visitactivities = 1
  //                         u.visitactivitiesdate = new Date()
  //                       } else if (Obj.visittype === 5) {
  //                         u.visitconditionstitle = 'บันทึกสภาพการตลาด'
  //                         u.visitconditions = 1
  //                         u.visitconditionsdate = new Date()
  //                       } else if (Obj.visittype === 6) {
  //                         u.visitcompetitorstitle = 'บันทึกคู่แข่ง'
  //                         u.visitcompetitors = 1
  //                         u.visitcompetitorsdate = new Date()
  //                       } else if (Obj.visittype === 7) {
  //                         u.visitactivitiespostpect = Obj.visitactivitiespostpect
  //                         u.visitpostpecttitle = 'บันทึกลูกค้าค้าดหวัง'
  //                         u.visitpostpect = 1
  //                         u.visitpostpectdate = new Date()
  //                         u.postpectname = Obj.custtomer
  //                       }
  //                       db.collection('resultappointment').update(q, {$set: u}, (err, rs) => {
  //                         if (err) throw err
  //                         resolve(appoint)
  //                       })
  //                     } else {
  //                       db.collection('resultappointment').insert(appoint, (err, rs) => {
  //                         if (err) throw err
  //                         resolve(appoint)
  //                       })
  //                     }
  //                   })
  //               })
  //           })
  //       })
  //     })
  //   })
  // },
  LoadResultAppointmentCollection: empcode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        let q = {visitcollection: 1, visitcollectionstatus: 0, empcode: empcode}
        db.collection('resultappointment').find(q)
          .toArray((er, res) => {
            resolve(res)
          })
      })
    })
  },
  LoadResultAppointmentCollectionUpdate: (resultcode, Obj) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var n = new Date()
        let q = {resultcode: resultcode}
        db.collection('resultappointment').update(q, {
          $set: {
            collectiondetail: Obj.cashtext,
            bankname: Obj.bankname,
            banknumber: Obj.banknumber,
            moneydate: new Date(),
            createdday: n.getDate(),
            createdmonth: n.getMonth() + 1,
            createdyear: n.getFullYear(),
            visitcollectionstatus: parseInt(1)
          }
        }, (er, res) => {
          assert.equal(err, null)
          resolve(res)
        })
      })
    })
  },
  IGetResultAppointmentCountAdd: (resultcode, newdate) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('resultappointment').update({resultcode: resultcode}, {$set: {coutdate: newdate}}, (er, res) => {
          assert.equal(err, null)
          resolve(res)
        })
      })
    })
  },
  IGetResultAppointmentCount: empcode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var n = new Date()
        var s = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-01T00:00:00Z'
        var e = n.getFullYear() + '-' + ('0' + (n.getMonth() + 1)).slice(-2) + '-31T00:00:00Z'
        db.collection('resultappointment').aggregate([
          {
            $match: {
              'created': {
                '$gte': new Date(s.toString()),
                '$lte': new Date(e.toString())
              },
              empcode: empcode,
              visit: 1
            }
          }, {
            $group: {
              _id: '$coutdate'
            }
          }
        ])
          .toArray((er, res) => {
            assert.equal(err, null)
            resolve(res)
          })
      })
    })
  }
}
