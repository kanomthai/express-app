import ENV from '../../config'
import Greeter from '../../config/greeter'
var guid = require('../../../assets/js/greeter')
var MongoClient = require('mongodb').MongoClient
const r = require('rethinkdb')
var assert = require('assert')
export default {
  InsAdjustmentSocial: (data, token) => {
    return new Promise(resolve => {
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        data.adjustmentcode = postnum
        data.approve = 0
        data.syncstatus = parseInt(0)
        data.created = new Date()
        data.adjustmenttype = 5
        data.adjustmentcredittype = 0
        data.adjustmentsocial = 1
        data.adjustmentcredit = 0
        data.adjustmentrename = 0
        data.adjustmenttransport = 0
        data.adjustmentaddress = 0
        data.txtcustomer = data.txtnumber + '-' + data.txtname
        db.collection('custtableadjustment').insert(data, (err, result) => {
          assert.equal(err, null)
          r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
            assert.equal(err, null)
            r.db('cloud').table('custtableadjustment').insert(data).run(conn, (err, res) => {
              assert.equal(err, null)
              resolve({
                result: result,
                adjust: data
              })
            })
          })
        })
      })
    })
  },
  InsAdjustCredit: (data, token) => {
    return new Promise(resolve => {
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey.err)
            }
            Greeter.CheckToken(token)
              .then(ykey => {
                var x = (y) => {
                  return parseFloat(y.replace(/,/g, ''))
                }
                data.adjustmentcode = postnum
                data.approve = 0
                data.newcredit = x(data.newcredit)
                data.syncstatus = 0
                data.empid = ykey.key
                data.created = new Date()
                data.adjustmenttype = 1
                data.adjustmentcredit = 1
                data.adjustmentrename = 0
                data.adjustmenttransport = 0
                data.adjustmentaddress = 0
                db.collection('custtableadjustment').insert(data, (err, result) => {
                  if (err) throw err
                  r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                    assert.equal(err, null)
                    r.db('cloud').table('custtableadjustment').insert(data).run(conn, (err, res) => {
                      assert.equal(err, null)
                      resolve({
                        result: result,
                        adjust: ykey
                      })
                    })
                  })
                })
              })
          })
      })
    })
  },
  InsAdjustAddress: (data, img, token) => {
    return new Promise(resolve => {
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey.err)
            }
            Greeter.CheckToken(token)
              .then(ykey => {
                data.adjustmentcode = postnum
                data.approve = 0
                data.syncstatus = 0
                data.empid = ykey.key
                data.created = new Date()
                data.adjustmenttype = 2
                data.adjustmentrename = 0
                data.adjustmentcredit = 0
                data.adjustmenttransport = 0
                data.adjustmentaddress = 1
                // data.transport = data.transport
                // data.telnotransport = data.telnotransport
                // data.remarktransport = data.remarktransport
                db.collection('custtableadjustment').insert(data, (err, result) => {
                  if (err) throw err
                  if (data.doc === true) {
                    r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                      assert.equal(err, null)
                      r.db('cloud').table('custtableadjustment').insert(data).run(conn, (err, res) => {
                        assert.equal(err, null)
                        resolve({
                          result: result,
                          adjust: ykey
                        })
                      })
                    })
                  } else {
                    img.img.doc3 = []
                    img.refid = postnum
                    img.created = new Date()
                    db.collection('annotetable').insert(img, (err, res) => {
                      if (err) throw err
                      resolve({
                        result: res,
                        adjust: ykey
                      })
                    })
                  }
                })
              })
          })
      })
    })
  },
  InsAdjustRename: (data, img, token) => {
    return new Promise(resolve => {
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey.err)
            }
            Greeter.CheckToken(token)
              .then(ykey => {
                // var x = (y) => {
                //   return parseFloat(y.replace(/,/g, ''))
                // }
                data.adjustmentcode = postnum
                data.approve = 0
                // data.newcredit = x(data.newcredit)
                data.syncstatus = 0
                data.empid = ykey.key
                data.created = new Date()
                data.adjustmenttype = 3
                data.adjustmentrename = 1
                data.adjustmentcredit = 0
                data.adjustmenttransport = 0
                data.adjustmentaddress = 0
                db.collection('custtableadjustment').insert(data, (err, result) => {
                  assert.equal(err, null)
                  resolve({
                    result: null,
                    adjust: ykey
                  })
                  // img.refid = postnum
                  // img.created = new Date()
                  // r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                  //   assert.equal(err, null)
                  //   r.db('cloud').table('custtableadjustment').insert(data).run(conn, (err, res) => {
                  //     assert.equal(err, null)
                  //     db.collection('annotetable').insert(img, (err, res) => {
                  //       if (err) throw err
                  //       resolve({
                  //         result: res,
                  //         adjust: ykey
                  //       })
                  //     })
                  //   })
                  // })
                })
              })
          })
      })
    })
  },
  InsAdjustTransport: (data, token) => {
    return new Promise(resolve => {
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey.err)
            }
            Greeter.CheckToken(token)
              .then(ykey => {
                data.adjustmentcode = postnum
                data.approve = 0
                data.syncstatus = 0
                data.empid = ykey.key
                data.created = new Date()
                data.adjustmenttype = 4
                data.adjustmentrename = 0
                data.adjustmentcredit = 0
                data.adjustmenttransport = 1
                data.adjustmentaddress = 0
                data.addressid = parseInt(data.addressid)
                db.collection('custtableadjustment').insert(data, (err, result) => {
                  if (err) throw err
                  r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
                    assert.equal(err, null)
                    r.db('cloud').table('custtableadjustment').insert(data).run(conn, (err, res) => {
                      assert.equal(err, null)
                      resolve({
                        result: result,
                        adjust: ykey
                      })
                    })
                  })
                })
              })
          })
      })
    })
  },
  UpdateAdjustMentCredit: (empcode, code, appr, remark, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var q = {
          adjustmentcode: code
        }
        db.collection('custtableadjustment')
          .find(q)
          .toArray((err, req) => {
            assert.equal(err, null)
            db.collection('custtableadjustment')
              .update(q, {$set: {approveby: empcode, approve: appr, remark: remark}}, (ee, rr) => {
                assert.equal(ee, null)
                // resolve(rr)
                db.collection('custtableadjustment')
                  .find(q).toArray((eee, rrz) => {
                    assert.equal(eee, null)
                    resolve(rrz)
                  })
              })
          })
      })
    })
  },
  getAdjustmentListfilter: (token, custmer) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(key => {
            if (key.err === true) {
              resolve(key)
            } else {
              var ter = key.territory
              var b = []
              ter.forEach(e => {
                b.push(e.territorylist[0].title)
              })
              console.dir(b)
              if (custmer !== null) {
                if (custmer.length > 0) {
                  db.collection('custtableadjustment')
                    .aggregate([
                      {
                        $match: {
                          territory: {
                            $in: b
                          },
                          approve: 0,
                          txtcustomer: {
                            $regex: custmer
                          }
                        }
                      },
                      {
                        $sort: {
                          created: -1
                        }
                      }
                    ])
                    .toArray((ee, re) => {
                      assert.equal(ee, null)
                      resolve(re)
                    })
                } else {
                  resolve([])
                }
              } else {
                db.collection('custtableadjustment')
                  .aggregate([
                    {
                      $match: {
                        territory: {
                          $in: b
                        },
                        approve: 0
                      }
                    },
                    // {
                    //   $lookup: {
                    //     from: 'annotetable',
                    //     localField: 'adjustmentcode',
                    //     foreignField: 'refid',
                    //     as: 'annote'
                    //   }
                    // },
                    {
                      $limit: 15
                    },
                    {
                      $sort: {
                        created: -1
                      }
                    }
                  ])
                  .toArray((ee, re) => {
                    assert.equal(ee, null)
                    resolve(re)
                  })
              }
            }
          })
      })
    })
  },
  getAdjustmentListTerminal: token => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(key => {
            if (key.err === true) {
              resolve(key)
            } else {
              var ter = key.territory
              console.dir(ter.territorylist)
              var b = []
              ter.forEach(e => {
                b.push(e.territorylist[0].title)
              })
              db.collection('custtableadjustment')
                .aggregate([
                  {
                    $match: {
                      territory: {
                        $in: b
                      },
                      approve: {
                        $in: [2, 0]
                      }
                    }
                  },
                  {
                    $limit: 10
                  },
                  {
                    $sort: {
                      created: -1
                    }
                  }
                ])
                .toArray((ee, re) => {
                  assert.equal(ee, null)
                  resolve(re)
                })
            }
          })
      })
    })
  },
  getAdjustmentList: token => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(key => {
            if (key.err === true) {
              resolve(key)
            } else {
              var ter = key.territory
              console.dir(ter.territorylist)
              var b = []
              ter.forEach(e => {
                b.push(e.territorylist[0].title)
              })
              console.dir(b)
              db.collection('custtableadjustment')
                .aggregate([
                  {
                    $match: {
                      territory: {
                        $in: b
                      },
                      approve: 0
                    }
                  },
                  {
                    $limit: 15
                  },
                  {
                    $sort: {
                      created: -1
                    }
                  }
                ])
                .toArray((ee, re) => {
                  assert.equal(ee, null)
                  resolve(re)
                })
            }
          })
      })
    })
  },
  insertPhone: (custnum, custname, telnumberold, telnumbernew, remark) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('custtableadjustmenttelno')
          .find({custnum: custnum})
          .toArray((err, req) => {
            assert.equal(err, null)
            if (req.length < 1) {
              db.collection('custtableadjustmenttelno')
                .insert({
                  custrecid: guid.GenCode(),
                  custnum: custnum,
                  custname: custname,
                  telnumberold: telnumberold,
                  telnumbernew: telnumbernew,
                  remark: remark,
                  syncstatus: 0,
                  created: new Date()
                }, (err, req) => {
                  assert.equal(err, null)
                  resolve()
                })
            } else {
              db.collection('custtableadjustmenttelno')
                .update({
                  custnum: custnum
                }, {
                  $set: {
                    telnumberold: telnumberold,
                    telnumbernew: telnumbernew,
                    remark: remark,
                    syncstatus: 0,
                    updated: new Date()
                  }
                }, (err, req) => {
                  assert.equal(err, null)
                  resolve()
                })
            }
          })
      })
    })
  },
  InsAdjustmentNew: Obj => {
    return new Promise(resolve => {
      const postnum = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Obj.adjustmentcode = postnum
        Obj.approve = 0
        Obj.syncstatus = 0
        Obj.created = new Date()
        db.collection('custtableadjustment').insert(Obj, (err, result) => {
          assert.equal(err, null)
          r.connect({ host: ENV.DB_RETHINK_HOST, port: ENV.DB_RETHINK_PORT }, (err, conn) => {
            assert.equal(err, null)
            r.db('cloud').table('custtableadjustment').insert(Obj).run(conn, (err, res) => {
              assert.equal(err, null)
              resolve(result)
            })
          })
        })
      })
    })
  }
}
