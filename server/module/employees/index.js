import ENV from '../../config'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
export default {
  ResetEmployeesList: (empid, password) => {
    return new Promise(resolve => {
      var query = {
        id: empid
      }
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('users').find(query)
          .toArray((err, r) => {
            assert.equal(err, null)
            if (r.length > 0) {
              db.collection('users').update(query, {
                $set: {
                  password: password,
                  created_at: new Date(),
                  updated_at: new Date()
                }
              }, (e, res) => {
                assert.equal(e, null)
                resolve({
                  success: true,
                  message: 'รีเซ็ตรหัสผ่านเรียบร้อยแล้ว!',
                  redirect: null
                })
              })
            } else {
              resolve({
                success: false,
                message: 'ไม่พบข้อมูลหรือข้อมูลไม่ถูกต้อง!',
                token: null,
                redirect: null
              })
            }
          })
      })
    })
  },
  GetEmployeesList: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('employeesinfomation')
          .aggregate([
            {
              $lookup: {
                from: 'users',
                localField: 'empid',
                foreignField: 'id',
                as: 'users'
              }
            },
            {
              $lookup: {
                from: 'masterposition',
                localField: 'position',
                foreignField: 'id',
                as: 'position'
              }
            },
            {
              $lookup: {
                from: 'masterterritory',
                localField: 'territory',
                foreignField: 'id',
                as: 'territory'
              }
            }
          ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetEmployees: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('users').find()
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetPosition: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('masterposition').find()
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetTerritory: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('masterterritory').find()
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetTerritoryLead: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('salesresponarea')
          .aggregate([
            {
              $lookup: {
                from: 'masterterritory',
                localField: 'leadid',
                foreignField: 'id',
                as: 'lead'
              }
            },
            // {
            //   $match: {filtername: {$regex: custnum}, salesdistrictid: territory}
            // },
            {
              $lookup: {
                from: 'masterterritory',
                localField: 'territory',
                foreignField: 'id',
                as: 'territory'
              }
            }
          ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  InsertTerritory: data => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var q = {leadid: data.leadid, territory: data.territory}
        db.collection('salesresponarea')
          .find(q)
          .toArray((err, result) => {
            assert.equal(err, null)
            if (result.length < 1) {
              db.collection('salesresponarea').insert(data, (err, rd) => {
                assert.equal(err, null)
                resolve(rd)
              })
            } else {
              db.collection('salesresponarea').update(q, {$set: data}, (err, rd) => {
                assert.equal(err, null)
                resolve(rd)
              })
            }
          })
      })
    })
  },
  InsertEmployees: data => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('employeesinfomation').find({empid: data[0].empid})
          .toArray((err, result) => {
            assert.equal(err, null)
            if (result.length < 1) {
              db.collection('employeesinfomation').insert(data, (err, result) => {
                assert.equal(err, null)
                resolve(result)
              })
            } else {
              console.dir(result[0].empid)
              var q = {empid: result[0].empid}
              var u = {position: result[0].position, territory: result[0].territory}
              db.collection('employeesinfomation').update(q, {$set: u}, (err, result) => {
                assert.equal(err, null)
                resolve(result)
              })
            }
          })
      })
    })
  },
  GetRegCusttable: (custnum, territory) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('custtable')
          .aggregate([
            {
              $lookup: {
                from: 'custtrans',
                localField: 'accountnum',
                foreignField: 'accountnum',
                as: 'balance'
              }
            },
            {
              $match: {filtername: {$regex: custnum}, salesdistrictid: territory}
            },
            {
              $lookup: {
                from: 'address',
                localField: 'partyid',
                foreignField: 'tableid',
                as: 'address'
              }
            }
          ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetBalanceCredit: custnum => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'accountnum': custnum }
        db.collection('custtrans').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  }
}
