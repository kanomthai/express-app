import ENV from '../../config'
// var c = require('colors')
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
// var mysql = require('mysql')
export default {
  GetOrder: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('salesorder').find({approve: 1})
          .toArray((err, rs) => {
            if (err) throw err
            // var connection = mysql.createConnection({
            //   host: ENV.MYSQL_HOST,
            //   user: ENV.MYSQL_USERS,
            //   password: ENV.MYSQL_PWD,
            //   database: ENV.MYSQL_DB_NAME
            // })
            resolve(rs)
          })
      })
    })
  }
}
