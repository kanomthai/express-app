import ENV from '../../config'
import greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var guid = require('../../../assets/js/greeter')
// var mysql = require('mysql')
var assert = require('assert')
export default {
  GetCusttableTerritoryList: (token) => {
    return new Promise(resolve => {
      greeter.GetTerritoryDetail(token)
        .then(key => {
          if (key.err === true) {
            resolve(key)
          } else {
            greeter.CheckTokenSales(key.key[0].territory[0].id)
              .then(rr => {
                // resolve(rr[0].salesdistrict)
                // console.dir(rr)
                var ter = rr
                var dSales = (te) => {
                  return new Promise(resolve => {
                    var b = []
                    for (let e in te) {
                      if (te[e].status === 1) {
                        b.push(te[e].salesdistrict[0].signature)
                      }
                    }
                    resolve(b)
                  })
                }
                dSales(ter).then(rd => {
                  resolve(rd)
                })
              })
          }
        })
    })
  },
  GetCusttableTerritory: (token) => {
    return new Promise(resolve => {
      greeter.GetTerritoryDetail(token)
        .then(key => {
          if (key.err === true) {
            resolve(key)
          } else {
            greeter.CheckTokenSales(key.key[0].territory[0].id)
              .then(rr => {
                // resolve(rr[0].salesdistrict)
                var ter = rr
                var b = []
                for (let e in ter) {
                  if (ter[e].status === 1) {
                    b.push(ter[e].salesdistrict[0].signature)
                  }
                }
                MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
                  assert.equal(err, null)
                  db.collection('custtable')
                    .aggregate([
                      {
                        $match: {
                          salesdistrictid: {
                            $in: b
                          }
                        }
                      },
                      {
                        $lookup: {
                          from: 'custtrans',
                          localField: 'accountnum',
                          foreignField: 'accountnum',
                          as: 'balance'
                        }
                      },
                      {
                        $lookup: {
                          from: 'custaddress',
                          localField: 'partyid',
                          foreignField: 'tableid',
                          as: 'address'
                        }
                      },
                      {
                        $lookup: {
                          from: 'custtablecreditdisc',
                          localField: 'accountnum',
                          foreignField: 'account',
                          as: 'creditdisc'
                        }
                      },
                      {
                        $lookup: {
                          from: 'custtablebackorder',
                          localField: 'accountnum',
                          foreignField: 'custnumber',
                          as: 'backorder'
                        }
                      },
                      {
                        $lookup: {
                          from: 'custtablebilling',
                          localField: 'accountnum',
                          foreignField: 'customernumber',
                          as: 'custtablebilling'
                        }
                      },
                      {
                        $limit: 15
                      }
                    ])
                    .toArray((err, result) => {
                      assert.equal(err, null)
                      resolve(result)
                    })
                })
              })
          }
        })
    })
  },
  GetRegCusttableBilling: territory => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('custtablebilling')
          .aggregate([
            {
              $match: {
                territory: territory,
                status: 0
              }
            },
            { $group: { _id: '$customernumber', total: { $sum: '$amount' } } },
            {
              $lookup: {
                from: 'custtable',
                localField: '_id',
                foreignField: 'accountnum',
                as: 'custtable'
              }
            },
            {
              $lookup: {
                from: 'custtablebilling',
                localField: '_id',
                foreignField: 'customernumber',
                as: 'custtablebilling'
              }
            },
            { $sort: { total: -1 } }
          ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetCust: territory => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('custtable')
          .aggregate([
            {
              $lookup: {
                from: 'custtrans',
                localField: 'accountnum',
                foreignField: 'accountnum',
                as: 'balance'
              }
            },
            {
              $match: {salesdistrictid: territory}
            },
            {
              $lookup: {
                from: 'address',
                localField: 'partyid',
                foreignField: 'tableid',
                as: 'address'
              }
            }
          ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetRegCusttableTerritory: (territory, token) => {
    return new Promise(resolve => {
      greeter.CheckTerritory(token)
        .then(key => {
          if (key.err === true) {
            resolve(key)
          } else {
            // console.log(key)
            var ter = key.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
              assert.equal(err, null)
              console.log('territory:' + territory)
              if (territory !== null) {
                db.collection('custtable')
                  .aggregate([
                    {
                      $match: {
                        salesdistrictid: territory
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtrans',
                        localField: 'accountnum',
                        foreignField: 'accountnum',
                        as: 'balance'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custaddress',
                        localField: 'partyid',
                        foreignField: 'tableid',
                        as: 'address'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablecreditdisc',
                        localField: 'accountnum',
                        foreignField: 'account',
                        as: 'creditdisc'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablebackorder',
                        localField: 'accountnum',
                        foreignField: 'custnumber',
                        as: 'backorder'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablebilling',
                        localField: 'accountnum',
                        foreignField: 'customernumber',
                        as: 'custtablebilling'
                      }
                    }
                  ])
                  .toArray((err, result) => {
                    assert.equal(err, null)
                    resolve(result)
                  })
              }
            })
          }
        })
    })
  },
  GetRegCusttable: (custnum, token) => {
    return new Promise(resolve => {
      greeter.CheckTerritory(token)
        .then(key => {
          if (key.err === true) {
            resolve(key)
          } else {
            // console.log(key)
            var ter = key.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
              assert.equal(err, null)
              console.log('custnum:' + custnum)
              if (custnum !== null) {
                db.collection('custtable')
                  .aggregate([
                    {
                      $match: {
                        filtername: {
                          $regex: custnum
                        }
                      }
                    },
                    {
                      $match: {
                        salesdistrictid: {
                          $in: b
                        }
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtrans',
                        localField: 'accountnum',
                        foreignField: 'accountnum',
                        as: 'balance'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custaddress',
                        localField: 'partyid',
                        foreignField: 'tableid',
                        as: 'address'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablecreditdisc',
                        localField: 'accountnum',
                        foreignField: 'account',
                        as: 'creditdisc'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablebackorder',
                        localField: 'accountnum',
                        foreignField: 'custnumber',
                        as: 'backorder'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablebilling',
                        localField: 'accountnum',
                        foreignField: 'customernumber',
                        as: 'custtablebilling'
                      }
                    }
                  ])
                  .toArray((err, result) => {
                    assert.equal(err, null)
                    if (result.length < 1) {
                      db.collection('custtable')
                        .aggregate([
                          {
                            $match: {
                              salesdistrictid: {
                                $in: b
                              }
                            }
                          },
                          {
                            $lookup: {
                              from: 'custtrans',
                              localField: 'accountnum',
                              foreignField: 'accountnum',
                              as: 'balance'
                            }
                          },
                          {
                            $lookup: {
                              from: 'custaddress',
                              localField: 'partyid',
                              foreignField: 'tableid',
                              as: 'address'
                            }
                          },
                          {
                            $lookup: {
                              from: 'custtablecreditdisc',
                              localField: 'accountnum',
                              foreignField: 'account',
                              as: 'creditdisc'
                            }
                          },
                          {
                            $lookup: {
                              from: 'custtablebackorder',
                              localField: 'accountnum',
                              foreignField: 'custnumber',
                              as: 'backorder'
                            }
                          },
                          {
                            $lookup: {
                              from: 'custtablebilling',
                              localField: 'accountnum',
                              foreignField: 'customernumber',
                              as: 'custtablebilling'
                            }
                          }
                        ])
                        .toArray((err, result) => {
                          assert.equal(err, null)
                          resolve(result)
                        })
                    } else {
                      resolve(result)
                    }
                  })
              } else {
                db.collection('custtable')
                  .aggregate([
                    {
                      $match: {
                        salesdistrictid: {
                          $in: b
                        }
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtrans',
                        localField: 'accountnum',
                        foreignField: 'accountnum',
                        as: 'balance'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custaddress',
                        localField: 'partyid',
                        foreignField: 'tableid',
                        as: 'address'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablecreditdisc',
                        localField: 'accountnum',
                        foreignField: 'account',
                        as: 'creditdisc'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablebackorder',
                        localField: 'accountnum',
                        foreignField: 'custnumber',
                        as: 'backorder'
                      }
                    },
                    {
                      $lookup: {
                        from: 'custtablebilling',
                        localField: 'accountnum',
                        foreignField: 'customernumber',
                        as: 'custtablebilling'
                      }
                    },
                    {
                      $limit: 15
                    }
                  ])
                  .toArray((err, result) => {
                    assert.equal(err, null)
                    resolve(result)
                  })
              }
            })
          }
        })
    })
  },
  GetBalanceCredit: custnum => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        assert.equal(err, null)
        var query = { 'accountnum': custnum }
        db.collection('custtrans').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetNewCustomer: token => {
    return new Promise(resolve => {
      // const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey)
            }
            var ter = rkey.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            db.collection('newcusttables')
              .aggregate([
                {
                  $match: {
                    sales: {
                      $in: b
                    }
                  }
                },
                {
                  $sort: {
                    updated: -1,
                    approvestatus: 1
                  }
                }
              ])
              .toArray((err, rs) => {
                assert.equal(err, null)
                resolve(rs)
              })
          })
      })
    })
  },
  CheckCustomerTxtIdNull: (txtid, territory, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('newcusttables')
          .aggregate([
            {
              $match: {
                'txtid': txtid
              }
            },
            {
              $lookup: {
                from: 'newcusttableaddress',
                localField: 'custrecid',
                foreignField: 'refid',
                as: 'addresslist'
              }
            },
            {
              $lookup: {
                from: 'newcusttablecontact',
                localField: 'custrecid',
                foreignField: 'refid',
                as: 'contactlist'
              }
            },
            {
              $lookup: {
                from: 'newcusttablecredit',
                localField: 'custrecid',
                foreignField: 'refid',
                as: 'creditlist'
              }
            },
            {
              $lookup: {
                from: 'newcusttablemarket',
                localField: 'custrecid',
                foreignField: 'refid',
                as: 'marketlist'
              }
            },
            {
              $lookup: {
                from: 'newcusttablemarketshare',
                localField: 'custrecid',
                foreignField: 'refid',
                as: 'marketsharelist'
              }
            },
            {
              $lookup: {
                from: 'newcusttablemeeting',
                localField: 'custrecid',
                foreignField: 'refid',
                as: 'meetinglist'
              }
            },
            // {
            //   $lookup: {
            //     from: 'newcusttabletransport',
            //     localField: 'custrecid',
            //     foreignField: 'refid',
            //     as: 'transportlist'
            //   }
            // },
            {
              $sort: {
                approvestatus: 1,
                updated: -1
              }
            }
          ])
          .toArray((err, req) => {
            assert.equal(err, null)
            resolve(req)
          })
      })
    })
  },
  GetCheckTxtId: (txtid, territory, token) => {
    return new Promise(resolve => {
      console.log('add new territory:' + territory)
      let n = new Date()
      let m = ('0' + n.getDate()).slice(-2) + '/' + ('0' + (n.getMonth() + 1)).slice(-2) + '/' + n.getFullYear()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            console.log('len:' + result.length)
            assert.equal(err, null)
            if (result.length < 1) {
              var c = guid.GenCode()
              var b = [{
                custrecid: c,
                txtid: txtid,
                txtname: null,
                txtcustname: null,
                txtdatebirth: m,
                txttelno: null,
                txtfax: null,
                txtemail: null,
                txtbussinesstype: 1,
                txtcustomertype: 1,
                strbussinesstype: 'สำนักงานใหญ่',
                strcustomertype: 'นิติบุคล',
                txtbussiness: null,
                create: new Date(),
                sales: territory,
                territorysales: territory
              }]
              db.collection('newcusttables').insert(b, (err, ress) => {
                assert.equal(err, null)
                db.collection('newcusttables')
                  .find(query)
                  .toArray((err, re) => {
                    assert.equal(err, null)
                    resolve(re)
                  })
              })
            } else {
              resolve(result)
            }
          })
      })
    })
  },
  GetCusttablesTxtId: (txtid, token, Obj) => {
    return new Promise(resolve => {
      if (Obj.txtid !== null) {
        greeter.CheckToken(token)
          .then(key => {
            if (key.err === true) {
              resolve(key)
            } else {
              MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
                assert.equal(err, null)
                var query = { 'txtid': txtid }
                Obj.sales = Obj.txtterritory
                console.log(Obj.txtdatebirth.substring(6, 10) + '\n')
                var u = new Date()
                if (parseInt(Obj.txtdatebirth.substr(6, 4)) > parseInt(u.getFullYear())) {
                  Obj.txtdatebirth = Obj.txtdatebirth.substr(0, 2) + '/' + Obj.txtdatebirth.substr(3, 2) + '/' + (parseInt(Obj.txtdatebirth.substr(6, 4)) - 543)
                } else {
                  Obj.txtdatebirth = Obj.txtdatebirth.substr(0, 2) + '/' + Obj.txtdatebirth.substr(3, 2) + '/' + Obj.txtdatebirth.substr(6, 4)
                }
                db.collection('newcusttables').find(query)
                  .toArray((err, result) => {
                    assert.equal(err, null)
                    if (result.length === 0) {
                      Obj.create = new Date()
                      db.collection('newcusttables').insert([Obj], (err, ress) => {
                        assert.equal(err, null)
                        resolve(ress)
                      })
                    } else {
                      Obj.update = new Date()
                      Obj.territory = new Date()
                      db.collection('newcusttables').update(query, {$set: Obj}, (err, ress) => {
                        assert.equal(err, null)
                        resolve(ress)
                      })
                    }
                  })
              })
            }
          })
      } else {
        resolve()
      }
    })
  },
  GetLoadAddress: (txtid, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            if (result.length > 0) {
              db.collection('newcusttableaddress').find({refid: result[0].custrecid})
                .toArray((err, rad) => {
                  assert.equal(err, null)
                  resolve(rad)
                })
            } else {
              resolve()
            }
            // console.dir(result[0].custrecid)
          })
      })
    })
  },
  GetUpdateAddress: (txtid, token, Obj, addresscode) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            var transp = Obj.transport
            if (Obj.addresstype !== 2) {
              transp = []
            }
            db.collection('newcusttableaddress').update(
              {refid: result[0].custrecid, addresscode: addresscode}, {
                $set: {
                  update: new Date(),
                  addresstype: Obj.addresstype,
                  txtcust: Obj.txtcust,
                  txtaddress: Obj.txtaddress,
                  txtamphor: Obj.txtamphor,
                  txtprovince: Obj.txtprovince,
                  txtzipcode: Obj.txtzipcode,
                  transport: transp
                }
              }, (err, rad) => {
                assert.equal(err, null)
                db.collection('newcusttableaddress').find({refid: result[0].custrecid})
                  .toArray((err, radue) => {
                    assert.equal(err, null)
                    resolve(radue)
                  })
              })
          })
      })
    })
  },
  InsCustomerAddressDelete: addresscode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('newcusttableaddress').remove({addresscode: addresscode}, (err, radue) => {
          assert.equal(err, null)
          resolve(radue)
        })
      })
    })
  },
  GetAddressTable: (txtid, token, Obj, addresstype) => {
    // console.dir(Obj)
    console.log('Obj.addall : ' + Obj.addall)
    return new Promise(resolve => {
      if (Obj.txtid !== null) {
        greeter.CheckToken(token)
          .then(key => {
            if (key.err === true) {
              resolve(key)
            } else {
              MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
                assert.equal(err, null)
                var query = { 'txtid': txtid }
                db.collection('newcusttables').find(query)
                  .toArray((err, result) => {
                    assert.equal(err, null)
                    console.dir(result[0].custrecid)
                    var b = []
                    if (Obj.addall === true) {
                      b.push({
                        refid: result[0].custrecid,
                        addresscode: guid.GenCode(),
                        addresstype: 1,
                        txtcust: Obj.txtcust,
                        txtaddress: Obj.txtaddress,
                        txtamphor: Obj.txtamphor,
                        txtprovince: Obj.txtprovince,
                        txtzipcode: Obj.txtzipcode,
                        transport: [],
                        created: new Date()
                      }, {
                        refid: result[0].custrecid,
                        addresscode: guid.GenCode(),
                        addresstype: 2,
                        txtcust: Obj.txtcust,
                        txtaddress: Obj.txtaddress,
                        txtamphor: Obj.txtamphor,
                        txtprovince: Obj.txtprovince,
                        txtzipcode: Obj.txtzipcode,
                        transport: Obj.transport,
                        created: new Date()
                      }, {
                        refid: result[0].custrecid,
                        addresscode: guid.GenCode(),
                        addresstype: 3,
                        txtcust: Obj.txtcust,
                        txtaddress: Obj.txtaddress,
                        txtamphor: Obj.txtamphor,
                        txtprovince: Obj.txtprovince,
                        txtzipcode: Obj.txtzipcode,
                        transport: [],
                        created: new Date()
                      })
                      db.collection('newcusttableaddress').insert(b, (err, ress) => {
                        assert.equal(err, null)
                        db.collection('newcusttableaddress').find({refid: result[0].custrecid})
                          .toArray((err, re) => {
                            assert.equal(err, null)
                            resolve(re)
                          })
                      })
                    } else {
                      console.log('addresstype :=> ' + addresstype)
                      var addresscode = Obj.addresscode
                      if (addresscode === null) {
                        addresscode = guid.GenCode()
                      }
                      var tt = Obj.transport
                      if (addresstype !== 2) {
                        tt = []
                      }
                      db.collection('newcusttableaddress')
                        .update(
                          { addresscode: addresscode }, {
                            $set: {
                              refid: result[0].custrecid,
                              addresscode: addresscode,
                              addresstype: addresstype,
                              txtcust: Obj.txtcust,
                              txtaddress: Obj.txtaddress,
                              txtamphor: Obj.txtamphor,
                              txtprovince: Obj.txtprovince,
                              txtzipcode: Obj.txtzipcode,
                              transport: tt,
                              updated: new Date()
                            }
                          }, {
                            upsert: true
                          },
                          (err, ress) => {
                            assert.equal(err, null)
                            db.collection('newcusttableaddress').find({refid: result[0].custrecid})
                              .toArray((err, re) => {
                                assert.equal(err, null)
                                resolve(re)
                              })
                          })
                    }
                  })
              })
            }
          })
      } else {
        resolve()
      }
    })
  },
  GetMeetingNewCust: (txtid, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            if (result.length < 1) {
              resolve(result)
            } else {
              db.collection('newcusttablemeeting').find({refid: result[0].custrecid})
                .toArray((err, re) => {
                  assert.equal(err, null)
                  if (re.length < 1) {
                    let gencode = guid.GenCode()
                    db.collection('newcusttablemeeting').insert([{
                      mecode: gencode,
                      refid: result[0].custrecid,
                      mdate: ['0', '1', '2', '3', '4', '5', '6'],
                      mday: ['วันอาทิตย์', 'วันจันทร์', 'วันอังคาร', 'วันพุธ', 'วันพฤหัส', 'วันศุกร์', 'วันเสาร์'],
                      mtimesstart: '08.00',
                      mtimesend: '18.00',
                      spedate: ['0', '1', '2', '3', '4', '5', '6'],
                      speday: ['วันอาทิตย์', 'วันจันทร์', 'วันอังคาร', 'วันพุธ', 'วันพฤหัส', 'วันศุกร์', 'วันเสาร์'],
                      spetimesstart: '08.00',
                      spetimesend: '18.00',
                      created: new Date()
                    }], (err, ress) => {
                      assert.equal(err, null)
                      db.collection('newcusttablemeeting').find({refid: result[0].custrecid})
                        .toArray((err, re) => {
                          assert.equal(err, null)
                          resolve(re)
                        })
                    })
                  } else {
                    db.collection('newcusttablemeeting').find({refid: result[0].custrecid})
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  }
                })
            }
          })
      })
    })
  },
  DelSocialMediaCustomerNew: code => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = {'socode': code}
        db.collection('newcusttablesocialmedia').remove(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablesocialmedia').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                resolve(re)
              })
          })
      })
    })
  },
  LoadSocialMediaCustomerNew: txtid => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = {'txtid': txtid}
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablesocialmedia').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                resolve(re)
              })
          })
      })
    })
  },
  UpdateSocialMediaCustomerNew: (txtid, Obj) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = {'txtid': txtid}
        db.collection('newcusttables').update(query, {$set: {
          social: Obj,
          updated: new Date()
        }}, (err, result) => {
          assert.equal(err, null)
          db.collection('newcusttables').find(query)
            .toArray((err, re) => {
              assert.equal(err, null)
              resolve(re)
            })
        })
      })
    })
  },
  UpdateTransportCustomerNew: (txtid, Obj, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = {'txtid': txtid}
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            // console.dir(result)
            db.collection('newcusttabletransport').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  let gencode = guid.GenCode()
                  db.collection('newcusttabletransport').insert([{
                    trancode: gencode,
                    refid: result[0].custrecid,
                    name: Obj.name,
                    comment: Obj.comment,
                    created: new Date()
                  }], (err, ress) => {
                    assert.equal(err, null)
                    db.collection('newcusttabletransport').find({refid: result[0].custrecid})
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  })
                } else {
                  db.collection('newcusttabletransport').update({refid: result[0].custrecid},
                    {$set: {
                      name: Obj.name,
                      telno: Obj.telno,
                      comment: Obj.comment,
                      updated: new Date()
                    }}, (err, ress) => {
                      assert.equal(err, null)
                      db.collection('newcusttabletransport').find({refid: result[0].custrecid})
                        .toArray((err, re) => {
                          assert.equal(err, null)
                          resolve(re)
                        })
                    })
                }
              })
          })
      })
    })
  },
  UpdateTransportCustomer: (txtid, Obj, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = {'txtid': txtid}
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            // console.dir(result)
            db.collection('newcusttabletransport').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  let gencode = guid.GenCode()
                  db.collection('newcusttabletransport').insert([{
                    trancode: gencode,
                    refid: result[0].custrecid,
                    name: Obj.name,
                    telno: Obj.telno,
                    comment: Obj.comment,
                    created: new Date()
                  }], (err, ress) => {
                    assert.equal(err, null)
                    db.collection('newcusttabletransport').find({refid: result[0].custrecid})
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  })
                } else {
                  db.collection('newcusttabletransport').find({refid: result[0].custrecid})
                    .toArray((err, re) => {
                      assert.equal(err, null)
                      resolve(re)
                    })
                  // db.collection('newcusttabletransport').update({refid: result[0].custrecid},
                  //   {$set: {
                  //     name: Obj.name,
                  //     telno: Obj.telno,
                  //     comment: Obj.comment,
                  //     updated: new Date()
                  //   }}, (err, ress) => {
                  //     assert.equal(err, null)
                  //     db.collection('newcusttabletransport').find({refid: result[0].custrecid})
                  //       .toArray((err, re) => {
                  //         assert.equal(err, null)
                  //         resolve(re)
                  //       })
                  //   })
                }
              })
          })
      })
    })
  },
  UpdateMeetingNewCust: (txtid, Obj, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            var b = ['วันอาทิตย์', 'วันจันทร์', 'วันอังคาร', 'วันพุธ', 'วันพฤหัส', 'วันศุกร์', 'วันเสาร์']
            var nday = []
            var sday = []
            for (let i in Obj.mdate) {
              nday.push(b[i])
            }
            for (let i in Obj.spedate) {
              sday.push(b[i])
            }
            db.collection('newcusttablemeeting').update({refid: result[0].custrecid}, {$set: {
              mdate: Obj.mdate,
              mday: nday,
              mtimesstart: Obj.mtimesstart,
              mtimesend: Obj.mtimesend,
              spedate: Obj.spedate,
              speday: sday,
              spetimesstart: Obj.spetimesstart,
              spetimesend: Obj.spetimesend,
              updated: new Date()
            }}, (err, ress) => {
              assert.equal(err, null)
              db.collection('newcusttablemeeting').find({refid: result[0].custrecid})
                .toArray((err, re) => {
                  assert.equal(err, null)
                  resolve(re)
                })
            })
          })
      })
    })
  },
  LoadMarketDataCustomer: (txtid, Obj, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablemarket')
              .aggregate([
                {
                  $match: {refid: result[0].custrecid}
                },
                {
                  $lookup: {
                    from: 'annotetable',
                    localField: 'refid',
                    foreignField: 'mkcode',
                    as: 'annote'
                  }
                }
              ])
              .toArray((err, re) => {
                assert.equal(err, null)
                resolve(re)
              })
          })
      })
    })
  },
  UpdateMarketDataCustomer: (txtid, Obj, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablemarket').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  let gencode = guid.GenCode()
                  db.collection('newcusttablemarket').insert([{
                    mkcode: gencode,
                    refid: result[0].custrecid,
                    statusplace: Obj.txtstatusplace,
                    totalap: Obj.txttotalap,
                    totalemp: Obj.txttotalemp,
                    bussiness: Obj.bussiness,
                    salestype: Obj.salestype,
                    created: new Date()
                  }], (err, ress) => {
                    assert.equal(err, null)
                    db.collection('newcusttablemarket')
                      .aggregate([
                        {
                          $match: {refid: result[0].custrecid}
                        },
                        {
                          $lookup: {
                            from: 'annotetable',
                            localField: 'refid',
                            foreignField: 'mkcode',
                            as: 'annote'
                          }
                        }
                      ])
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  })
                } else {
                  // console.dir(Obj.bussiness)
                  let u = {
                    statusplace: Obj.txtstatusplace,
                    totalap: Obj.txttotalap,
                    totalemp: Obj.txttotalemp,
                    bussiness: Obj.bussiness,
                    salestype: Obj.salestype,
                    updated: new Date()
                  }
                  db.collection('newcusttablemarket').update({refid: result[0].custrecid}, {$set: u}, (err, ress) => {
                    assert.equal(err, null)
                    db.collection('newcusttablemarket')
                      .aggregate([
                        {
                          $match: {refid: result[0].custrecid}
                        },
                        {
                          $lookup: {
                            from: 'annotetable',
                            localField: 'refid',
                            foreignField: 'mkcode',
                            as: 'annote'
                          }
                        }
                      ])
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  })
                }
              })
          })
      })
    })
  },
  LoadCustCredit: (txtid, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablecredit').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  let gencode = guid.GenCode()
                  db.collection('newcusttablecredit').insert([{
                    cdcode: gencode,
                    refid: result[0].custrecid,
                    creditid: 0,
                    txtcredit: null,
                    txttotalcash: 0,
                    typecash: 0,
                    txttypecash: null,
                    bankid: 0,
                    bankname: null,
                    banknumber: null,
                    comment: null,
                    created: new Date()
                  }], (err, ress) => {
                    assert.equal(err, null)
                    db.collection('newcusttablecredit')
                      .find({refid: result[0].custrecid})
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  })
                } else {
                  resolve(re)
                }
              })
          })
      })
    })
  },
  UpdateCustCredit: (txtid, Obj, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablecredit').find({refid: result[0].custrecid})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  let gencode = guid.GenCode()
                  db.collection('newcusttablecredit').insert([{
                    cdcode: gencode,
                    refid: result[0].custrecid,
                    creditid: Obj.creditid,
                    txttypecash: Obj.txttypecash,
                    bankid: Obj.bankid,
                    txtcredit: Obj.txtcredit,
                    txttotalcash: Obj.txttotalcash,
                    typecash: Obj.typecash,
                    bankname: Obj.bankname,
                    banknumber: Obj.banknumber,
                    comment: Obj.comment,
                    created: new Date()
                  }], (err, ress) => {
                    assert.equal(err, null)
                    db.collection('newcusttablecredit')
                      .find({refid: result[0].custrecid})
                      .toArray((err, re) => {
                        assert.equal(err, null)
                        resolve(re)
                      })
                  })
                } else {
                  // console.dir(Obj.bussiness)
                  var c = {
                    creditid: Obj.creditid,
                    txttypecash: Obj.txttypecash,
                    bankid: Obj.bankid,
                    txtcredit: Obj.txtcredit,
                    txttotalcash: Obj.txttotalcash,
                    typecash: Obj.typecash,
                    bankname: Obj.bankname,
                    banknumber: Obj.banknumber,
                    comment: Obj.comment,
                    updated: new Date()
                  }
                  db.collection('newcusttablecredit')
                    .update({refid: result[0].custrecid}, {$set: c}, (err, ress) => {
                      assert.equal(err, null)
                      db.collection('newcusttablecredit')
                        .find({refid: result[0].custrecid})
                        .toArray((err, re) => {
                          assert.equal(err, null)
                          resolve(re)
                        })
                    })
                }
              })
          })
      })
    })
  },
  LoadContact: (txtid, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablecontact')
              .aggregate([
                {
                  $match: {refid: result[0].custrecid}
                },
                {
                  $lookup: {
                    from: 'annotetable',
                    localField: 'ccode',
                    foreignField: 'refid',
                    as: 'annote'
                  }
                }])
              .toArray((err, re) => {
                assert.equal(err, null)
                resolve(re)
              })
          })
      })
    })
  },
  insertContact: (txtid, Obj, gCode, pathname, img, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablecontact').find({refid: result[0].custrecid, ccode: gCode})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  var b = [{
                    refid: result[0].custrecid,
                    ccode: gCode,
                    txtname: Obj.txtname,
                    txttelno: Obj.txttelno,
                    txtemptype: Obj.txtemptype,
                    pathname: Obj.pathname,
                    created: new Date()
                  }]
                  db.collection('newcusttablecontact').insert(b, (err, rsult) => {
                    assert.equal(err, null)
                    db.collection('annotetable').insert({
                      recid: guid.GenCode(),
                      refid: gCode,
                      doctype: 2,
                      type: 0,
                      title: 'เอกสารผู้ติดต่อ',
                      img: img,
                      syncstatus: 0,
                      created: new Date()
                    }, (err, requi) => {
                      assert.equal(err, null)
                      db.collection('newcusttablecontact')
                        .aggregate([
                          {
                            $match: {refid: result[0].custrecid}
                          },
                          {
                            $lookup: {
                              from: 'annotetable',
                              localField: 'ccode',
                              foreignField: 'refid',
                              as: 'annote'
                            }
                          }])
                        .toArray((err, rds) => {
                          assert.equal(err, null)
                          resolve(rds)
                        })
                    })
                  })
                } else {
                  var u = {
                    txtname: Obj.txtname,
                    txttelno: Obj.txttelno,
                    txtemptype: Obj.txtemptype,
                    updated: new Date()
                  }
                  db.collection('newcusttablecontact').update({ccode: gCode}, {$set: u}, (err, rsult) => {
                    assert.equal(err, null)
                    db.collection('annotetable').update({refid: gCode}, {$set: {img: img, updated: new Date()}}, (err, requi) => {
                      assert.equal(err, null)
                      db.collection('newcusttablecontact')
                        .aggregate([
                          {
                            $match: {refid: result[0].custrecid}
                          },
                          {
                            $lookup: {
                              from: 'annotetable',
                              localField: 'ccode',
                              foreignField: 'refid',
                              as: 'annote'
                            }
                          }])
                        .toArray((err, rds) => {
                          assert.equal(err, null)
                          resolve(rds)
                        })
                    })
                  })
                }
              })
          })
      })
    })
  },
  LoadImgAnnote: (txtid, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        console.log(query)
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            if (result.length > 0) {
              db.collection('annotetable')
                .find({refid: result[0].custrecid, doctype: 0})
                .toArray((err, re) => {
                  assert.equal(err, null)
                  resolve(re)
                })
            } else {
              resolve()
            }
          })
      })
    })
  },
  LoadMarketShr: (txtid, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            if (result.length > 0) {
              db.collection('newcusttablemarketshare')
                .find({refid: result[0].custrecid})
                .toArray((err, re) => {
                  assert.equal(err, null)
                  resolve(re)
                })
            } else {
              resolve()
            }
          })
      })
    })
  },
  InsertMarketShr: (txtid, Obj, gCode, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables').find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttablemarketshare').find({refid: result[0].custrecid, mcode: gCode})
              .toArray((err, re) => {
                assert.equal(err, null)
                if (re.length < 1) {
                  var b = [{
                    refid: result[0].custrecid,
                    mcode: gCode,
                    txtbussiness: Obj.txtbussiness,
                    txtcontact: Obj.txtcontact,
                    txttelno: Obj.txttelno,
                    txtqty: Obj.txtqty,
                    txttypebussiness: Obj.txttypebussiness,
                    syncstatus: 0,
                    created: new Date()
                  }]
                  db.collection('newcusttablemarketshare').insert(b, (err, rsult) => {
                    assert.equal(err, null)
                    db.collection('newcusttablemarketshare')
                      .find({refid: result[0].custrecid})
                      .toArray((err, rds) => {
                        assert.equal(err, null)
                        resolve(rds)
                      })
                  })
                } else {
                  var u = {
                    txtbussiness: Obj.txtbussiness,
                    txtcontact: Obj.txtcontact,
                    txttelno: Obj.txttelno,
                    txtqty: Obj.txtqty,
                    txttypebussiness: Obj.txttypebussiness,
                    updated: new Date()
                  }
                  db.collection('newcusttablemarketshare').update({mcode: gCode}, {$set: u}, (err, rsult) => {
                    assert.equal(err, null)
                    db.collection('newcusttablemarketshare')
                      .find({refid: result[0].custrecid})
                      .toArray((err, rds) => {
                        assert.equal(err, null)
                        resolve(rds)
                      })
                  })
                }
              })
          })
      })
    })
  },
  CheckNewCustomerAnnote: txtid => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables')
          .find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('annotetable')
              .find({refid: result[0].custrecid})
              .toArray((ee, reg) => {
                assert.equal(ee, null)
                resolve(reg)
              })
          })
      })
    })
  },
  CheckNewCustomerAddress: txtid => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        db.collection('newcusttables')
          .find(query)
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('newcusttableaddress')
              .find({refid: result[0].custrecid})
              .toArray((ee, reg) => {
                assert.equal(ee, null)
                resolve(reg)
              })
          })
      })
    })
  },
  GetRegCusttableTerritoryList: (custnum, territory, territoryid) => {
    return new Promise(resolve => {
      greeter.CheckTokenSales(parseInt(territoryid))
        .then(rr => {
          var ter = rr
          var b = []
          // console.dir(ter)
          for (let e in ter) {
            if (ter[e].status === 1) {
              b.push(ter[e].salesdistrict[0].signature)
            }
          }
          // console.dir(b)
          MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
            assert.equal(err, null)
            if (custnum === null || custnum.length < 1) {
              db.collection('custtable')
                .aggregate([
                  {
                    $match: {
                      salesdistrictid: {
                        $in: [territory]
                      }
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtrans',
                      localField: 'accountnum',
                      foreignField: 'accountnum',
                      as: 'balance'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custaddress',
                      localField: 'partyid',
                      foreignField: 'tableid',
                      as: 'address'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtablecreditdisc',
                      localField: 'accountnum',
                      foreignField: 'account',
                      as: 'creditdisc'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtablebackorder',
                      localField: 'accountnum',
                      foreignField: 'custnumber',
                      as: 'backorder'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtablebilling',
                      localField: 'accountnum',
                      foreignField: 'customernumber',
                      as: 'custtablebilling'
                    }
                  },
                  {
                    $limit: 5
                  }
                ])
                .toArray((err, result) => {
                  assert.equal(err, null)
                  resolve(result)
                })
            } else {
              db.collection('custtable')
                .aggregate([
                  {
                    $match: {
                      salesdistrictid: {
                        $in: b
                      },
                      $or: [
                        {
                          filtername: {
                            $regex: custnum
                          }
                        }
                      ]
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtrans',
                      localField: 'accountnum',
                      foreignField: 'accountnum',
                      as: 'balance'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custaddress',
                      localField: 'partyid',
                      foreignField: 'tableid',
                      as: 'address'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtablecreditdisc',
                      localField: 'accountnum',
                      foreignField: 'account',
                      as: 'creditdisc'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtablebackorder',
                      localField: 'accountnum',
                      foreignField: 'custnumber',
                      as: 'backorder'
                    }
                  },
                  {
                    $lookup: {
                      from: 'custtablebilling',
                      localField: 'accountnum',
                      foreignField: 'customernumber',
                      as: 'custtablebilling'
                    }
                  }
                ])
                .toArray((err, result) => {
                  assert.equal(err, null)
                  resolve(result)
                })
            }
          })
        })
    })
  },
  UpsCustomer: (txtid, comment, st, token, empcode) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var query = { 'txtid': txtid }
        var up = {
          syncstatus: 0,
          approveby: empcode,
          approvestatus: st,
          txtcomment: comment,
          update: new Date(),
          updated: new Date()
        }
        // db.collection('newcusttables').update(query, {$set: {syncstatus: 0, approvestatus: st, approveby: empcode, txtcomment: comment, update: new Date()}}, (err, result) => {
        db.collection('newcusttables').update(query, {$set: up}, (err, result) => {
          assert.equal(err, null)
          if (st === 2) {
            db.collection('newcusttables')
              .aggregate([
                {
                  $match: {
                    'txtid': txtid
                  }
                },
                {
                  $lookup: {
                    from: 'newcusttableaddress',
                    localField: 'custrecid',
                    foreignField: 'refid',
                    as: 'addresslist'
                  }
                },
                {
                  $lookup: {
                    from: 'newcusttablecontact',
                    localField: 'custrecid',
                    foreignField: 'refid',
                    as: 'contactlist'
                  }
                },
                {
                  $lookup: {
                    from: 'newcusttablecredit',
                    localField: 'custrecid',
                    foreignField: 'refid',
                    as: 'creditlist'
                  }
                },
                {
                  $lookup: {
                    from: 'newcusttablemarket',
                    localField: 'custrecid',
                    foreignField: 'refid',
                    as: 'marketlist'
                  }
                },
                {
                  $lookup: {
                    from: 'newcusttablemarketshare',
                    localField: 'custrecid',
                    foreignField: 'refid',
                    as: 'marketsharelist'
                  }
                },
                {
                  $lookup: {
                    from: 'newcusttablemeeting',
                    localField: 'custrecid',
                    foreignField: 'refid',
                    as: 'meetinglist'
                  }
                },
                // {
                //   $lookup: {
                //     from: 'newcusttabletransport',
                //     localField: 'custrecid',
                //     foreignField: 'refid',
                //     as: 'transportlist'
                //   }
                // },
                {
                  $sort: {
                    approvestatus: 1,
                    updated: -1
                  }
                }
              ])
              .toArray((err, req) => {
                assert.equal(err, null)
                resolve(req)
              })
          } else {
            resolve(result)
          }
        })
      })
    })
  }
}
