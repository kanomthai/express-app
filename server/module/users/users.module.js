
import ENV from '../../config'
var assert = require('assert')
var mysql = require('mysql')
var jwt = require('jsonwebtoken')
var MongoClient = require('mongodb').MongoClient
// var url = 'mongodb://' + ENV.MongoClient_HOST + ':' + ENV.MongoClient_PORT + '/' + ENV.MongoClient_DB_CLOUD_NAME
var url = 'mongodb://://127.0.0.1:27017/cloud'

export default {
  CheckUsers: (email, password) => {
    return new Promise(resolve => {
      var n = new Date()
      let xm = n.getMonth() - 2
      if (xm < 0) {
        xm = 0
      }
      var s = n.getFullYear() + '-' + ('0' + xm).slice(-2) + '-' + '01' + 'T00:00:00Z'
      var e = n.getFullYear() + '-' + ('0' + n.getMonth()).slice(-2) + '-' + ('0' + (n.getDate() + 1)).slice(-2) + 'T12:00:00Z'
      var query = {
        email: email,
        password: password
      }
      console.dir(s)
      console.dir(e)
      var queryFilter = {
        email: email,
        password: password,
        updated_at: {
          '$gte': new Date(s.toString())
        }
      }
      var b
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('users').find(query)
          .toArray((err, r) => {
            assert.equal(err, null)
            if (r.length > 0) {
              db.collection('users').find(queryFilter)
                .toArray((err, rsdte) => {
                  assert.equal(err, null)
                  console.dir(rsdte)
                  if (rsdte.length > 0) {
                    b = {
                      id: r[0].id,
                      name: r[0].name,
                      email: r[0].email,
                      empcode: r[0].empcode,
                      role: r[0].role,
                      actived: r[0].actived
                    }
                    var token = jwt.sign(b, ENV.SECRET_KEY, {
                      expiresIn: 86400 // expires in 24h * 3d
                    })
                    db.collection('users').update(query, {$set: {updated_at: new Date(), remember_token: token}}, (e, res) => {
                      assert.equal(e, null)
                      resolve({
                        success: true,
                        message: 'เข้าสู่ระบบเรียบร้อยแล้ว!',
                        token: token,
                        user: b,
                        redirect: null
                      })
                    })
                  } else {
                    resolve({
                      success: false,
                      message: 'รหัสผ่านหมดอายุ!',
                      token: null,
                      user: b,
                      redirect: '/auth/pwd'
                    })
                  }
                })
            } else {
              resolve({
                success: false,
                message: 'ไม่พบข้อมูลหรือข้อมูลไม่ถูกต้อง!',
                token: null,
                user: [],
                redirect: null
              })
            }
          })
      })
    })
  },
  CheckUsersPwd: (email, password) => {
    return new Promise(resolve => {
      var query = {
        email: email
      }
      var b
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('users').find(query)
          .toArray((err, r) => {
            assert.equal(err, null)
            if (r.length > 0) {
              b = {
                id: r[0].id,
                name: r[0].name,
                email: r[0].email,
                empcode: r[0].empcode,
                role: r[0].role,
                actived: r[0].actived
              }
              var token = jwt.sign(b, ENV.SECRET_KEY, {
                expiresIn: 86400 // expires in 24h * 3d
              })
              db.collection('users').update(query, {$set: {password: password, created_at: new Date(), updated_at: new Date(), remember_token: token}}, (e, res) => {
                assert.equal(e, null)
                resolve({
                  success: true,
                  message: ' ตั้งรหัสผ่านเรียบร้อยแล้ว!',
                  token: token,
                  user: b,
                  redirect: null
                })
              })
            } else {
              resolve({
                success: false,
                message: 'ไม่พบข้อมูลหรือข้อมูลไม่ถูกต้อง!',
                token: null,
                user: [],
                redirect: null
              })
            }
          })
      })
    })
  },
  SyncUsersToMongoClientDB: () => {
    return new Promise(resolve => {
      var conn = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      conn.connect()
      conn.query(`select * from users`, (err, rows, fields) => {
        assert.equal(err, null)
        var r = rows
        r.forEach((data, i) => {
          console.log(data.empcode)
          MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
            assert.equal(err, null)
            var query = {empcode: data.empcode}
            db.collection('users').find(query)
              .toArray((err, rs) => {
                assert.equal(err, null)
                if (rs.length > 0) {
                  // update
                  var updata = {
                    name: data.name,
                    email: data.email,
                    updated_at: new Date(),
                    role: data.role,
                    actived: data.actived
                  }
                  db.collection('users').update(query, {$set: updata})
                    .then((error, result) => {
                      assert.equal(error, null)
                      console.log(result)
                    })
                }
              })
          })
        })
        resolve(rows)
      })
      conn.end()
    })
  }
}
