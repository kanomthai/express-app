import ENV from '../../config'
// import greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
// var guid = require('../../../assets/js/greeter')
// var mysql = require('mysql')
var assert = require('assert')
export default {
  GetClaimList: (empcode, territory, customer) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        console.dir(customer)
        if (customer === null) {
          db.collection('orderclaim')
            .aggregate([
              {
                $match: {
                  createby: empcode
                }
              },
              {
                $sort: {
                  created_at: -1
                }
              },
              {
                $limit: 25
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        } else {
          db.collection('orderclaim')
            .aggregate([
              {
                $match: {
                  filtername: {
                    $regex: customer
                  }
                }
              },
              {
                $sort: {
                  created_at: -1
                }
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        }
      })
    })
  }
}
