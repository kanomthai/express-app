import ENV from '../../config'
import Greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
// var guid = require('../../../assets/js/greeter')

export default {
  GetVisitPostPect: token => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey)
            }
            var ter = rkey.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            // console.dir(b)
            db.collection('resultappointment')
              .aggregate([
                {
                  $match: {
                    salesdistrictid: {
                      $in: b
                    },
                    visittype: 8
                  }
                },
                {
                  $sort: {
                    created: -1
                  }
                }
              ])
              .toArray((err, result) => {
                assert.equal(err, null)
                resolve(result)
              })
          })
      })
    })
  },
  GetPostPect: (filter, token) => {
    return new Promise(resolve => {
      // const appkey = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        Greeter.CheckTerritory(token)
          .then(rkey => {
            if (rkey.err === true) {
              resolve(rkey)
            }
            var ter = rkey.territory
            var b = []
            ter.forEach(e => {
              b.push(e.territorylist[0].title)
            })
            if (filter !== null) {
              db.collection('commercialcustomers')
                .aggregate([
                  {
                    $match: {
                      name: {
                        $regex: filter
                      },
                      salesdistrictid: {
                        $in: b
                      }
                    }
                  }
                ])
                .toArray((err, rs) => {
                  assert.equal(err, null)
                  if (rs.length < 1) {
                    db.collection('commercialcustomers')
                      .aggregate([
                        {
                          $match: {
                            salesdistrictid: {
                              $in: b
                            }
                          }
                        }
                      ])
                      .toArray((err, rse) => {
                        assert.equal(err, null)
                        resolve(rse)
                      })
                  }
                  resolve(rs)
                })
            } else {
              db.collection('commercialcustomers')
                .aggregate([
                  {
                    $match: {
                      salesdistrictid: {
                        $in: b
                      }
                    }
                  }
                ])
                .toArray((err, rse) => {
                  assert.equal(err, null)
                  resolve(rse)
                })
            }
          })
      })
    })
  }
}
