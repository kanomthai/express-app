import ENV from '../../config'
// import greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
// var guid = require('../../../assets/js/greeter')
// var mysql = require('mysql')
var assert = require('assert')
export default {
  InsAnnoteNewAccount: (txtid, Obj, type, token) => {
    return new Promise(resolve => {
      // var c = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('newcusttables')
          .updateMany({txtid: txtid}, {$set: {
            pathname: Obj
          }}, (e, rq) => {
            assert.equal(e, null)
            resolve({
              err: false,
              msg: '1 document inserted',
              req: rq
            })
          })
      })
    })
  },
  // InsAnnoteNewAccount: (txtid, obj, title, type, doctype, recid, token) => {
  //   return new Promise(resolve => {
  //     // var c = guid.GenCode()
  //     MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
  //       assert.equal(err, null)
  //       db.collection('newcusttables')
  //         .find({txtid: txtid})
  //         .toArray((err, result) => {
  //           assert.equal(err, null)
  //           db.collection('annotetable')
  //             .insert([{
  //               doctype: doctype,
  //               recid: recid,
  //               refid: result[0].custrecid,
  //               type: type,
  //               title: title,
  //               img: obj,
  //               created: new Date()
  //             }], (e, rq) => {
  //               assert.equal(e, null)
  //               db.close()
  //               resolve({
  //                 err: false,
  //                 msg: '1 document inserted',
  //                 req: rq
  //               })
  //             })
  //         })
  //     })
  //   })
  // },
  InsAnnote: (txtid, obj, title, type, doctype, recid, token) => {
    return new Promise(resolve => {
      // var c = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('newcusttables')
          .find({txtid: txtid})
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('annotetable')
              .insert([{
                doctype: doctype,
                recid: recid,
                refid: result[0].custrecid,
                type: type,
                title: title,
                img: obj,
                created: new Date()
              }], (e, rq) => {
                assert.equal(e, null)
                db.close()
                resolve({
                  err: false,
                  msg: '1 document inserted',
                  req: rq
                })
              })
          })
      })
    })
  },
  InsAnnoteAdd: (Obj, token) => {
    return new Promise(resolve => {
      // var c = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('annotetable')
          .insert(Obj, (e, rq) => {
            assert.equal(e, null)
            db.close()
            resolve(rq)
          })
      })
    })
  },
  AnnoteDelNewAccount: (recid, token) => {
    return new Promise(resolve => {
      console.log('ssr recid:' + recid.toUpperCase())
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var myquery = { recid: recid.toUpperCase() }
        db.collection('annotetable').deleteOne(myquery, (err, obj) => {
          assert.equal(err, null)
          console.log('1 document deleted')
          db.close()
          resolve({
            err: false,
            msg: '1 document deleted',
            req: obj
          })
        })
      })
    })
  },
  DelAnnote: (recid, token) => {
    return new Promise(resolve => {
      console.log('ssr recid:' + recid.toUpperCase())
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        var myquery = { recid: recid.toUpperCase() }
        db.collection('annotetable').deleteOne(myquery, (err, obj) => {
          assert.equal(err, null)
          console.log('1 document deleted')
          db.close()
          resolve({
            err: false,
            msg: '1 document deleted',
            req: obj
          })
        })
      })
    })
  },
  InsAnnoteMarket: (txtid, obj, title, type, doctype, refid, recid, token) => {
    return new Promise(resolve => {
      // var c = guid.GenCode()
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('newcusttables')
          .find({txtid: txtid})
          .toArray((err, result) => {
            assert.equal(err, null)
            db.collection('annotetable')
              .insert([{
                doctype: doctype,
                recid: recid,
                refid: refid,
                type: type,
                title: title,
                img: obj,
                created: new Date()
              }], (e, rq) => {
                assert.equal(e, null)
                db.close()
                resolve({
                  err: false,
                  msg: '1 document inserted',
                  req: rq
                })
              })
          })
      })
    })
  },
  InsAnnotePicNewAccount: (refid, type) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('annotetable').find({refid: refid, type: type})
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  InsAnnotePic: (refid, type) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('annotetable').find({refid: refid, type: type})
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  }
  // GetCust: territory => {
  //   return new Promise(resolve => {
  //     MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
  //       if (err) throw err
  //       assert.equal(err, null)
  //       db.collection('custtable')
  //         .aggregate([
  //           {
  //             $lookup: {
  //               from: 'custtrans',
  //               localField: 'accountnum',
  //               foreignField: 'accountnum',
  //               as: 'balance'
  //             }
  //           },
  //           {
  //             $match: {salesdistrictid: territory}
  //           },
  //           {
  //             $lookup: {
  //               from: 'address',
  //               localField: 'partyid',
  //               foreignField: 'tableid',
  //               as: 'address'
  //             }
  //           }
  //         ])
  //         .toArray((err, result) => {
  //           if (err) throw err
  //           resolve(result)
  //         })
  //     })
  //   })
  // }
}
