import ENV from '../../config'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
export default {
  InsertNewInventory: (e) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('inventtable').replaceOne({
          recid: e.recid,
          itemid: e.itemid
        }, {
          recid: e.recid,
          itemid: e.itemid,
          itemname: e.itemname,
          date_stock_in: e.datestockin,
          stockstatus: e.stockstatus,
          amount: parseFloat(e.amount),
          filtername: e.itemid + '' + e.itemname,
          stockstatusclass: e.stockstatusclass,
          unitid: e.unitid,
          stop: parseInt(e.stop),
          price: parseFloat(e.amount),
          datestock: e.datestock,
          create_by: "admin",
          update_by: "admin",
          created_at: new Date(),
          updated_at: new Date(),
          quantityamount: e.quantityamount,
          img: 'https://bulma.io/images/placeholders/128x128.png',
          itemtype: 1,
          salestype: parseInt(e.salestype),
          salesremark: e.salesremark
        }, { upsert: true }, (err, result) => {
          if (err) {
            resolve({
              error: true,
              result: []
            })
          } else {
            resolve({
              error: false,
              result: result
            })
          }
        })
      })
    })
  },
  GetInventTableList: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('inventtable')
          .aggregate([
            {
              $lookup: {
                from: 'inventtableprice',
                localField: 'itemid',
                foreignField: 'itemid',
                as: 'inventtableprice'
              }
            },
            {
              $limit: 5
            }
          ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetInventTableByNam: (filtername) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        if (filtername === null || filtername.length < 1) {
          db.collection('inventtable')
            .aggregate([
              {
                $lookup: {
                  from: 'inventtableprice',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtableprice'
                }
              },
              {
                $limit: 5
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        } else {
          db.collection('inventtable')
            .aggregate([
              {
                $match: {
                  filtername: {$regex: filtername}
                }
              },
              {
                $lookup: {
                  from: 'inventtableprice',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtableprice'
                }
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        }
      })
    })
  },
  GetInventTable: (filter, custnum, groupid) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        if (filter === null) {
          db.collection('inventtableprice')
            .aggregate([
              {
                $match: {
                  $or: [
                    {
                      itemcust: custnum
                    }, {
                      accountrelation: groupid
                    }
                  ]
                }
              },
              {
                $lookup: {
                  from: 'inventtable',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtable'
                }
              },
              {
                $lookup: {
                  from: 'yssitemspecialtable',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'itemspecial'
                }
              },
              {
                $limit: 15
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        } else {
          db.collection('inventtableprice')
            .aggregate([
              {
                $match: {
                  filtername: {
                    $regex: filter // '79C115S343A5-D'
                  },
                  $or: [
                    {
                      itemcust: custnum
                    }, {
                      accountrelation: groupid
                    }
                  ]
                }
              },
              {
                $lookup: {
                  from: 'inventtable',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtable'
                }
              },
              {
                $lookup: {
                  from: 'yssitemspecialtable',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'itemspecial'
                }
              }
            ])
            .toArray((err, result) => {
              if (err) throw err
              resolve(result)
            })
        }
      })
    })
  },
  GetInventTableSpecial: (filter) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        if (filter === null) {
          db.collection('inventtable')
            .aggregate([
              {
                $limit: 5
              },
              {
                $lookup: {
                  from: 'inventtableprice',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtableprice'
                }
              }
            ])
            .toArray((err, result) => {
              if (err) throw err
              resolve(result)
            })
        } else {
          db.collection('inventtable')
            .aggregate([
              {
                $match: {
                  filtername: {
                    $regex: filter
                  }
                }
              },
              {
                $lookup: {
                  from: 'inventtableprice',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtableprice'
                }
              }
            ])
            .toArray((err, result) => {
              if (err) throw err
              resolve(result)
            })
        }
      })
    })
  },
  GetInventTableByNameETC: (filter, ordertype) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        let dcol = db.collection('ysschockdiscounttable')
        if (ordertype === 9) {
          dcol = db.collection('yssmotogangdisctable')
        } else if (ordertype === 8) {
          dcol = db.collection('yssmotoduodisctable')
        } else if (ordertype === 7) {
          dcol = db.collection('yssduodiscounttable')
        } else if (ordertype >= 6) {
          dcol = db.collection('yssspringdiscounttable')
        } else {
          dcol = db.collection('ysschocksiscounttable')
        }
        dcol.aggregate([
          {
            $limit: 200
          }
        ])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetInventTableByNameSPCList: (filter) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        if (filter !== null) {
          db.collection('inventtablespcrear')
            .aggregate([
              {
                $match: {
                  itemid: filter
                }
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        } else {
          resolve([])
        }
      })
    })
  },
  GetInventTableByNameSPC: (filter, typeitem) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        console.log(typeitem)
        if (filter === null) {
          db.collection('inventtablespc')
            .aggregate([
              {
                $match: {
                  typeitem: parseInt(typeitem)
                }
              },
              {
                $lookup: {
                  from: 'inventtableprice',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtableprice'
                }
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        } else {
          db.collection('inventtablespc')
            .aggregate([
              {
                $match: {
                  filtername: {
                    $regex: filter
                  },
                  typeitem: parseInt(typeitem)
                }
              },
              {
                $lookup: {
                  from: 'inventtableprice',
                  localField: 'itemid',
                  foreignField: 'itemid',
                  as: 'inventtableprice'
                }
              }
            ])
            .toArray((err, result) => {
              assert.equal(err, null)
              resolve(result)
            })
        }
      })
    })
  }
}
