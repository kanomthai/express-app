import ENV from '../../config'
// import greeter from '../../config/greeter'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
export default {
  GetProvince: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('masterprovince')
          .aggregate([
            {
              $lookup: {
                from: 'masteramphoes',
                localField: 'pid',
                foreignField: 'changwat_pid',
                as: 'masteramphoes'
              }
            }
          ])
          .toArray((err, result) => {
            if (err) throw err
            resolve(result)
          })
      })
    })
  },
  LoadBank: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('masterbank').find()
          .toArray((e, r) => {
            if (e) throw e
            resolve(r)
          })
      })
    })
  },
  LoadBilling: (custnumber) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('custtablebilling').find({customernumber: custnumber, status: 0})
          .toArray((e, r) => {
            if (e) throw e
            resolve(r)
          })
      })
    })
  },
  LoadMasterActivities: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('masteractivities').find({show: true})
          .toArray((e, r) => {
            assert.equal(e, null)
            resolve(r)
          })
      })
    })
  },
  LoadState: (custnumber) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('custtablebilling').find({customernumber: custnumber, status: 0})
          .toArray((e, r) => {
            if (e) throw e
            resolve(r)
          })
      })
    })
  },
  LoadCity: (custnumber) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        if (err) throw err
        assert.equal(err, null)
        db.collection('custtablebilling').find({customernumber: custnumber, status: 0})
          .toArray((e, r) => {
            if (e) throw e
            resolve(r)
          })
      })
    })
  },
  LoadCityZipcode: zipcode => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('addresszipcode').find({zipcode: zipcode, status: 0})
          .toArray((e, r) => {
            if (e) throw e
            resolve(r)
          })
      })
    })
  },
  LoadMasterSocial: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('mastersocial').find({status: true})
          .toArray((e, r) => {
            assert.equal(e, null)
            resolve(r)
          })
      })
    })
  },
  LoadCityZipcodeAll: () => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('addresszipcode')
          .aggregate([
            {
              $match: {
                status: 0
              }
            },
            {
              $lookup: {
                from: 'addressstate',
                localField: 'state',
                foreignField: 'stateid',
                as: 'addressstate'
              }
            }
          ])
          .toArray((e, r) => {
            assert.equal(err, null)
            resolve(r)
          })
      })
    })
  }
}
