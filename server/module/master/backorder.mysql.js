import ENV from '../../config'
var mysql = require('mysql')
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')
export default {
  GetBackOrder: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query('select * from cloudbackorder', (err, rows, fields) => {
        if (err) throw err
        // console.log('The solution is: ', rows.legth)
        resolve(rows)
      })
      connection.end()
    })
  },
  GetBilling: () => {
    return new Promise(resolve => {
      var connection = mysql.createConnection({
        host: ENV.MYSQL_HOST,
        user: ENV.MYSQL_USERS,
        password: ENV.MYSQL_PWD,
        database: ENV.MYSQL_DB_NAME
      })
      connection.connect()
      connection.query('select * from custtablebilling', (err, rows, fields) => {
        if (err) throw err
        // resolve(rows)
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          if (err) throw err
          assert.equal(err, null)
          db.collection('custtablebilling').insert(rows, (e, r) => {
            if (e) throw e
            resolve(r)
          })
        })
      })
      connection.end()
    })
  }
}
