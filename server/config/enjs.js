var CryptoJS = require('crypto-js')
const key = 'ed26eff67bdb11eccb0f84421bbed74d'
export default {
  EnCypto: (txt) => {
    // Encrypt
    var ciphertext = CryptoJS.AES.encrypt(txt, key)
    return ciphertext
  },
  DeCypto: (ciphertext) => {
    var bytes = CryptoJS.AES.decrypt(ciphertext.toString(), key)
    var plaintext = bytes.toString(CryptoJS.enc.Utf8)
    return plaintext.toString()
  }
}
