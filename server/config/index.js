import cypt from './enjs'
// import ENV_DB from './dev.env'
const MYSQL_HOST = cypt.DeCypto('U2FsdGVkX19ql0uzLN+ra9XPBr/YU92QPw1MsT/qgus=') // 'U2FsdGVkX1/FA2AG0AeRTXKj5YJS/c/oaH13fm3VD6I=' // '203.154.115.225'//
const MYSQL_USERS = cypt.DeCypto('U2FsdGVkX1/F7lF8v1Hu9jfnOw4SVHtQmyBLeqFJhFM=')
const MYSQL_PWD = cypt.DeCypto('U2FsdGVkX1+RcioJWMd62WUguV1Nerlx0A8S6dzWM95WM7nNDFTIGqbhknNd3F96')
const MYSQL_DB_NAME = cypt.DeCypto('U2FsdGVkX19lx334wR4p/hzRkIzbp0x64EbOJ+GUH70=') // 'test_service' //
const MSSQL_HOST = cypt.DeCypto('U2FsdGVkX19EUVIyLwbLFBKiTSaTfTgqnQLlN2/S42U=')
const MSSQL_USERS = cypt.DeCypto('U2FsdGVkX19bms/vrS2OfCbuIYuKjJ2jbvuG6jJZAm0=')
const MSSQL_PWD = cypt.DeCypto('U2FsdGVkX19Q1eF6Z3n4VMcn7W6crCBKiRkQ/89ugZo=')
const MSSQL_DB_NAME = cypt.DeCypto('U2FsdGVkX1/QuZW7K5yfsq2IxgdRKxDWygGhHHwJkWM=')
// const MONGO_HOST = cypt.DeCypto('U2FsdGVkX1/FA2AG0AeRTXKj5YJS/c/oaH13fm3VD6I=')
const DB_RETHINK_HOST = '192.168.2.1'
const DB_RETHINK_PORT = 28015
var MONGO_HOST = '192.168.2.1'
const MONGO_USERS = cypt.DeCypto('U2FsdGVkX19bms/vrS2OfCbuIYuKjJ2jbvuG6jJZAm0=')
const MONGO_PWD = cypt.DeCypto('U2FsdGVkX19Q1eF6Z3n4VMcn7W6crCBKiRkQ/89ugZo=')
const MONGO_DB_NAME = cypt.DeCypto('U2FsdGVkX1+S/6s9LaZIjzYay29eW/t03trrhQ2F0ok=')
const MONGO_DB_CLOUD_NAME = cypt.DeCypto('U2FsdGVkX1/OKJLLP8dZKqZb6UHspKe1LSTf1QqR908=')
const MONGO_PORT = cypt.DeCypto('U2FsdGVkX19CMMnBpz2L7eCzdTC1PLTBxqJiQVtSUw8=')
const API_MAIL = cypt.DeCypto('U2FsdGVkX18u0cc5t8iJ+c09Uf2XpmCEJ034Au5Mdz4N8C10cnAOn5wXKn/9YcZ5')
const API_MAIL_PWD = cypt.DeCypto('U2FsdGVkX19vENm4PMh65s9QLSbAsoTnsxrdzU7Ys0k=')
const SECRET_KEY = cypt.DeCypto('U2FsdGVkX1/Bwh3Bosrt9hKvEjd9mwJMP6+Zj1+FfQ7Xik3MPQgMH6vhKJmlSxJMcK6uLAFBGsQAkoRlJFZOkw==')
const LineToken = 'Zb7A421UVT8bLFgCJJ3ESP5FGWP1df14mPoKC9JG3IP'
// const LineToken = 'hSJUkH5cUgdjiZNgvdcJz6xgpRKlpGZoGMVAfotlu7n'
const LineTokenApprove = 'hSJUkH5cUgdjiZNgvdcJz6xgpRKlpGZoGMVAfotlu7n'

/* config data */
const LevelApprove = 8
const LevelSupApprove = 6

export default {
  MYSQL_HOST,
  MYSQL_USERS,
  MYSQL_PWD,
  MYSQL_DB_NAME,
  MSSQL_HOST,
  MSSQL_USERS,
  MSSQL_PWD,
  MSSQL_DB_NAME,
  MONGO_HOST,
  MONGO_USERS,
  MONGO_PWD,
  MONGO_DB_NAME,
  MONGO_DB_CLOUD_NAME,
  MONGO_PORT,
  SECRET_KEY,
  LineToken,
  LineTokenApprove,
  LevelApprove,
  LevelSupApprove,
  API_MAIL,
  API_MAIL_PWD,
  DB_RETHINK_HOST,
  DB_RETHINK_PORT
}
