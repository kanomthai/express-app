import jwt from 'jsonwebtoken'
import ENV from './index'
import axios from 'axios'
var assert = require('assert')
var MongoClient = require('mongodb').MongoClient
const GetTerritory = (token) => {
  return new Promise(resolve => {
    jwt.verify(token, ENV.SECRET_KEY, (err, success) => {
      if (err) {
        resolve({
          err: true,
          message: 'ถูกตัดการเชื่อมต่อแล้ว',
          key: null,
          redirecto: '/auth/login'
        })
      } else {
        // var empid = success.id
        var empid = success.id
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          assert.equal(err, null)
          if (db) {
            db.collection('employeesinfomation')
              .aggregate([
                {
                  $match: { empid: empid }
                },
                {
                  $lookup: {
                    from: 'masterposition',
                    localField: 'position',
                    foreignField: 'id',
                    as: 'position'
                  }
                },
                {
                  $lookup: {
                    from: 'masterterritory',
                    localField: 'territory',
                    foreignField: 'id',
                    as: 'territory'
                  }
                }
              ])
              .toArray((err, result) => {
                assert.equal(err, null)
                resolve({
                  err: false,
                  message: 'token ใช้งานได้ปกติ',
                  key: result
                })
              })
          } else {
            resolve({
              err: true,
              message: 'ติดต่อ database ไม่ได้',
              key: []
            })
          }
        })
        // MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        //   assert.equal(err, null)

        // })
      }
    })
  })
}

function getToken (token) {
  return new Promise(resolve => {
    jwt.verify(token, ENV.SECRET_KEY, (err, success) => {
      if (err) {
        resolve({
          err: true,
          message: 'ถูกตัดการเชื่อมต่อแล้ว',
          key: null,
          redirecto: '/auth/login'
        })
      } else {
        getEmployeesInfo(token).then(rd => {
          var position = null
          if (rd.key[0].position[0].id > ENV.LevelApprove) {
            position = 'display: none'
          } else {
            position = null
          }
          MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
            assert.equal(err, null)
            resolve({
              err: false,
              message: 'token ใช้งานได้ปกติ',
              key: success,
              position: position,
              level: rd.key[0].position[0].id,
              approvelevel: ENV.LevelApprove,
              redirecto: null
            })
          })
        })
      }
    })
  })
}

function getEmployeesInfo (token) {
  return new Promise(resolve => {
    jwt.verify(token, ENV.SECRET_KEY, (err, success) => {
      if (err) {
        resolve({
          err: true,
          message: 'ถูกตัดการเชื่อมต่อแล้ว',
          key: null,
          redirecto: '/auth/login'
        })
      } else {
        var empid = success.id
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          assert.equal(err, null)
          if (db) {
            db.collection('employeesinfomation')
              .aggregate([
                {
                  $match: { empid: empid }
                },
                {
                  $lookup: {
                    from: 'masterposition',
                    localField: 'position',
                    foreignField: 'id',
                    as: 'position'
                  }
                },
                {
                  $lookup: {
                    from: 'masterterritory',
                    localField: 'territory',
                    foreignField: 'id',
                    as: 'territory'
                  }
                }
              ])
              .toArray((err, result) => {
                if (err) throw err
                db.collection('salesresponarea')
                  .aggregate([
                    {
                      $match: {
                        territory: result[0].territory[0].id,
                        leadid: {$ne: result[0].territory[0].id},
                        status: 1
                      }
                    },
                    {
                      $lookup: {
                        from: 'masterterritory',
                        localField: 'territory',
                        foreignField: 'id',
                        as: 'territorylist'
                      }
                    }
                  ])
                  .toArray((err, rs) => {
                    if (err) throw err
                    resolve({
                      err: false,
                      message: 'token ใช้งานได้ปกติ',
                      key: result,
                      territory: rs,
                      redirecto: null
                    })
                  })
              })
          } else {
            resolve({
              err: true,
              message: 'ถูกตัดการเชื่อมต่อแล้ว',
              key: null,
              redirecto: '/auth/login'
            })
          }
        })
      }
    })
  })
}

function getMailLead (lead) {
  return new Promise(resolve => {
    MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
      assert.equal(err, null)
      if(db){
        db.collection('employeesinfomation')
          .aggregate([
            {
              $match: { territory: { $in: lead } }
            },
            {
              $lookup: {
                from: 'users',
                localField: 'empid',
                foreignField: 'id',
                as: 'email'
              }
            }
          ])
          .toArray((err, result) => {
            if (err) throw err
            resolve({
              err: false,
              message: 'token ใช้งานได้ปกติ',
              detail: result,
              redirecto: null
            })
          })
      }
    })
  })
}

function getEmployeesInfoLead (token) {
  return new Promise(resolve => {
    jwt.verify(token, ENV.SECRET_KEY, (err, success) => {
      if (err) {
        resolve({
          err: true,
          message: 'ถูกตัดการเชื่อมต่อแล้ว',
          key: null,
          redirecto: '/auth/login'
        })
      } else {
        var empid = success.id
        MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
          assert.equal(err, null)
          db.collection('employeesinfomation')
            .aggregate([
              {
                $match: { empid: empid }
              },
              {
                $lookup: {
                  from: 'masterposition',
                  localField: 'position',
                  foreignField: 'id',
                  as: 'position'
                }
              },
              {
                $lookup: {
                  from: 'masterterritory',
                  localField: 'territory',
                  foreignField: 'id',
                  as: 'territory'
                }
              }
            ])
            .toArray((err, result) => {
              if (err) throw err
              db.collection('salesresponarea')
                .aggregate([
                  {
                    $match: { leadid: result[0].territory[0].id, status: 1 }
                  },
                  {
                    $lookup: {
                      from: 'masterterritory',
                      localField: 'territory',
                      foreignField: 'id',
                      as: 'territorylist'
                    }
                  }
                ])
                .toArray((err, rs) => {
                  if (err) throw err
                  resolve({
                    err: false,
                    message: 'token ใช้งานได้ปกติ',
                    key: result,
                    territory: rs,
                    redirecto: null
                  })
                })
            })
        })
      }
    })
  })
}
export default {
  EnCodeCypto: code => {
    return new Promise(resolve => {
      var CryptoJS = require('crypto-js')
      var ciphertext = CryptoJS.AES.encrypt(code, ENV.SECRET_KEY)
      resolve(ciphertext)
    })
  },
  DeCodeCypto: code => {
    return new Promise(resolve => {
      var CryptoJS = require('crypto-js')
      var bytes = CryptoJS.AES.decrypt(code.toString(), ENV.SECRET_KEY)
      var plaintext = bytes.toString(CryptoJS.enc.Utf8)
      resolve(plaintext)
    })
  },
  CheckTokenSales: (tokenid) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('salesresponarea')
          .aggregate([
            {
              $match: {leadid: tokenid}
            },
            {
              $lookup: {
                from: 'masterterritory',
                localField: 'leadid',
                foreignField: 'id',
                as: 'lead'
              }
            },
            {
              $lookup: {
                from: 'masterterritory',
                localField: 'territory',
                foreignField: 'id',
                as: 'salesdistrict'
              }
            }])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  CheckTokenSalesTerritoryId: (territoryid) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        db.collection('employeesinfomation')
          .aggregate([
            {
              $match: {territory: territoryid}
            },
            {
              $lookup: {
                from: 'masterterritory',
                localField: 'territory',
                foreignField: 'id',
                as: 'territory'
              }
            },
            {
              $lookup: {
                from: 'users',
                localField: 'empid',
                foreignField: 'id',
                as: 'employee'
              }
            }])
          .toArray((err, result) => {
            assert.equal(err, null)
            resolve(result)
          })
      })
    })
  },
  GetTerritoryDetail: token => {
    return new Promise(resolve => {
      GetTerritory(token).then(r => resolve(r))
    })
  },
  CheckToken: token => {
    return new Promise(resolve => {
      getToken(token).then(r => {
        GetTerritory(token).then(rd => resolve({emp: r, terr: rd}))
      })
    })
  },
  CheckTerritory: token => {
    return new Promise(resolve => {
      getEmployeesInfoLead(token).then(r => {
        resolve(r)
      })
    })
  },
  updateBackOrder: e => {
    return new Promise(resolve => {
      var b = []
      var query = { recid: e.recid }
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        db.collection('custtablebackorder')
          .find(query)
          .toArray((er, s) => {
            if (err) throw err
            if (s.length > 0) {
              db.collection('custtablebackorder')
                .updateOne(query, {$set: {
                  recid: e.recid,
                  soid: e.soid,
                  custname: e.custname,
                  custnumber: e.custnumber,
                  itemid: e.itemid,
                  itemname: e.itemname,
                  stockstatus: e.stockstatus,
                  stockclass: e.stockclass,
                  qty: parseInt(e.qty),
                  price: parseFloat(e.price),
                  itempercent: parseFloat(e.itempercent),
                  areaid: e.areaid,
                  status: parseInt(e.status),
                  pricepercent: parseFloat(e.pricepercent),
                  amount: parseFloat(e.amount),
                  updated_at: new Date()
                }}, (e, res) => {
                  if (e) throw e
                  resolve()
                })
            } else {
              b.push({
                id: parseInt(e.id),
                recid: e.recid,
                soid: e.soid,
                custname: e.custname,
                custnumber: e.custnumber,
                itemid: e.itemid,
                itemname: e.itemname,
                stockstatus: e.stockstatus,
                stockclass: e.stockclass,
                qty: parseInt(e.qty),
                price: parseFloat(e.price),
                itempercent: parseFloat(e.itempercent),
                areaid: e.areaid,
                status: parseInt(e.status),
                pricepercent: parseFloat(e.pricepercent),
                amount: parseFloat(e.amount),
                created_at: new Date(),
                updated_at: null
              })
              db.collection('custtablebackorder')
                .insertOne(b, (e, res) => {
                  if (e) throw e
                  b.length = 0
                  resolve()
                })
            }
          })
      })

    })
  },
  GetGPS: () => {
    return new Promise(resolve => {
      axios.get('http://freegeoip.net/json/')
        .then(r => {
          resolve(r)
        })
    })
  },
  SendMailNodeJS: (mForm, token) => {
    return new Promise(resolve => {
      let nodemailer = require('nodemailer')
      const account = {
        user: ENV.API_MAIL,
        pass: ENV.API_MAIL_PWD
      }
      getToken(token).then(r => {
        getEmployeesInfo(token).then(rd => {
          var evar = rd.territory
          var lead = []
          evar.forEach(nn => {
            lead.push(nn.leadid)
          })
          getMailLead(lead).then(ed => {
            var conntactmail = ed.detail
            var mailto = []
            conntactmail.forEach(nm => {
              mailto.push(nm.email)
            })
            var transporter = nodemailer.createTransport({
              host: 'mail.yss.co.th',
              port: 587,
              secure: true,
              auth: {
                user: account.user,
                pass: account.pass
              }
            })
            var mailOptions = {
              from: r.key.email, // 'developer@yss.co.th',
              to: 'krumii.it@gmail.com',
              subject: mForm.title,
              html: mForm.body + '<br /><a href="http://app.yssasia.com/approves/adjustment">ตรวจสอบข้อมูล</a>'
            }
            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                console.log(error)
              } else {
                console.log('Email sent: ' + info.response)
                resolve({msg: info.response, lead: ed.detail})
              }
            })
          })
        })
      })
    })
  },
  checkImgRecid: (code, token) => {
    return new Promise(resolve => {
      MongoClient.connect(url, (err, db) => {
        assert.equal(err, null)
        db.collection('annotetable')
          .aggregate([
            {
              $match: {
                recid: code
              }
            }
          ])
          .toArray((err, rs) => {
            assert.equal(err, null)
            resolve(rs)
          })
      })
    })
  },
  checkImgRefid: (code, token) => {
    return new Promise(resolve => {
      MongoClient.connect('mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME, (err, db) => {
        assert.equal(err, null)
        db.collection('annotetable')
          .aggregate([
            {
              $match: {
                refid: code
              }
            }
          ])
          .toArray((err, rs) => {
            assert.equal(err, null)
            resolve(rs)
          })
      })
    })
  }
}
