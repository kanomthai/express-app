import ENV from './index'
var assert = require('assert')
var mongodb = require('mongodb')
var MongoClient = mongodb.MongoClient
var url = 'mongodb://' + ENV.MONGO_HOST + ':' + ENV.MONGO_PORT + '/' + ENV.MONGO_DB_CLOUD_NAME
export default {
    start: () => {
        return new Promise(resolve => {
            MongoClient.connect(url, (err, db) => {
                assert.equal(null, err)
                console.log("Successfully connected to mongo")
                resolve(db)
            })
        })
    }
}
