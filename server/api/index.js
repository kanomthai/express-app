import { Router } from 'express'
import { Auth,
  Users,
  Employees,
  Master,
  Custtable,
  SyncMysqlToMongo,
  InvenTables,
  Orders,
  Nottifies,
  ResultAppointment,
  ResultPostpect,
  AdjustMent,
  SendMailRouter,
  Annote,
  Reports,
  Appointments,
  Claim
} from '../router'
const app = Router()
// Add USERS Routes
app.use(Users)
app.use('/auth', Auth)
app.use('/employees', Employees)
app.use('/master', Master)
app.use('/custtable', Custtable)
app.use('/inventtable', InvenTables)
app.use('/orders', Orders)
app.use('/line', Nottifies)
app.use('/resultappointment', ResultAppointment)
app.use('/postpect', ResultPostpect)
app.use('/adjustments', AdjustMent)
app.use('/mailer', SendMailRouter)
app.use('/annote', Annote)
app.use('/reports', Reports)
app.use('/appointment', Appointments)
app.use('/claim', Claim)

/* Sync Router */
app.use('/sync', SyncMysqlToMongo)
export default app
