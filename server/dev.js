/* jshint node: true */
import ENV from './config'
import express from 'express'
import { Nuxt, Builder } from 'nuxt'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import session from 'express-session'
import compression from 'compression'
import methodOverride from 'method-override'
// import cluster from 'cluster'
import cluster from 'express-cluster'
import uuid from 'node-uuid'
import api from './api'
import ENV_DB from './config/dev.env'
var cpuCount = require('os').cpus().length
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')
const app = express()
const host = '0.0.0.0'
const port = process.env.PORT || 3000 || 3001 || 3002
app.use(compression())
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }))
app.use(bodyParser.json({limit: '10mb'}))
morgan.token('id', function getId (req) {
    return req.id
})
var assignId = (req, res, next) => {
    req.id = uuid.v4()
    next()
}
app.use(assignId)
app.use(morgan(':id :method :url :response-time'))
app.use(cookieParser())
app.use(session({
    secret: ENV.SECRET_KEY,
    resave: true,
    saveUninitialized: true
}))
app.use(methodOverride())
app.use('/api', api)
const nuxt = new Nuxt(config)
if (config.dev) {
    const builder = new Builder(nuxt)
    builder.build()
}
app.use(nuxt.render)
app.listen(port, host)
console.log('Server listening on ' + host + ':' + port)
