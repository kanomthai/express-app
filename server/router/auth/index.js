import { Router } from 'express'
import MonGo from '../../controller/mongo.module'
import greeter from '../../config/greeter'

const app = new Router()
app.post('/login', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.CheckUsers(req.body.email.toLowerCase(), req.body.password)
    .then(r => res.json({ empcode: req.body.empcode, password: req.body.password, data: r }))
})
app.post('/change/pwd', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.CheckUsersPwd(req.body.email.toLowerCase(), req.body.password)
    .then(r => res.json({ empcode: req.body.empcode, password: req.body.password, data: r }))
})
app.get('/verify', (req, res, next) => {
  const token = req.headers['x-access-token']
  greeter.CheckToken(token)
    .then(rs => res.json({data: rs}))
})
app.post('/verify/leadid', (req, res, next) => {
  const token = req.headers['x-access-token']
  greeter.CheckTokenSales(req.body.tokenid, token)
    .then(rs => res.json({data: rs}))
})
app.post('/verify/territoryid', (req, res, next) => {
  greeter.CheckTokenSalesTerritoryId(req.body.territoryid).then(rs => res.json(rs))
})
app.post('/verify/images', (req, res, next) => {
  greeter.checkImgRecid(req.body.code).then(rs => res.json(rs))
})
app.post('/verify/images/refid', (req, res, next) => {
  greeter.checkImgRefid(req.body.code).then(rs => res.json(rs))
})
export default app
