import { Router } from 'express'
import Oreports from '../../controller/mongo.module'
const app = new Router()
app.get('/', (req, res, next) => {
  Oreports.getViewReports(req.headers['x-access-token']).then(r => res.json(r))
})

app.post('/transfer', (req, res, next) => {
  Oreports.LoadResultAppointmentCollection(req.body.empcode).then(r => res.json(r))
})
app.post('/transfer/update', (req, res, next) => {
  Oreports.LoadResultAppointmentCollectionUpdate(req.body.resultcode, req.body.data).then(r => res.json(r))
})

app.post('/activities', (req, res, next) => {
  Oreports.LoadResultAppointmentActivities(req.body.empcode).then(r => res.json(r))
})
app.post('/market', (req, res, next) => {
  Oreports.LoadResultAppointmentActivitiesMarket(req.body.empcode).then(r => res.json(r))
})
export default app
