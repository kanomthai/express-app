import { Router } from 'express'
import MonGo from '../../controller/mongo.module'

const app = new Router()

app.post('/load/data', (req, res, next) => {
  var territory = req.body.territory
  var status = req.body.status
  MonGo.GetAppointsApprove(territory, status).then(r => res.json(r))
})
app.post('/load', (req, res, next) => {
  var territory = req.body.territory
  MonGo.AppointsLoad(territory).then(r => res.json(r))
})

app.post('/find/appointment', (req, res, next) => {
  MonGo.GetRegCusttableTerritoryAppointments(req.body.territory, req.body.empcode).then(r => res.json(r))
})
app.post('/load/new', (req, res, next) => {
  var territory = req.body.territory
  MonGo.AppointsLoadNew(territory).then(r => res.json(r))
})
app.post('/load/count', (req, res, next) => {
  var territory = req.body.territory
  MonGo.GroupCountAppointments(territory).then(r => res.json(r))
})
app.post('/count', (req, res, next) => {
  var territory = req.body.territory
  MonGo.GroupAppointments(territory).then(r => res.json(r))
})
app.post('/add', (req, res, next) => {
  // var token = req.headers['x-access-token']
  var empcode = req.body.empcode
  var territory = req.body.territory
  var data = req.body.data
  MonGo.AppointsInsData(empcode, territory, data).then(r => res.json(r))
})
app.post('/check', (req, res, next) => {
  var appointmentcode = req.body.appointmentcode
  MonGo.AppointsCheckStatus(appointmentcode).then(r => res.json(r))
})
app.post('/copy', (req, res, next) => {
  var appointmentcode = req.body.appointmentcode
  var territory = req.body.territory
  var empcode = req.body.empcode
  MonGo.AppointsCopy(appointmentcode, territory, empcode).then(r => res.json(r))
})
app.post('/status', (req, res, next) => {
  var appointmentcode = req.body.appointmentcode
  var status = req.body.status
  var remark = req.body.remark
  MonGo.AppointsApprove(appointmentcode, status, remark).then(r => res.json(r))
})
app.post('/status/territory', (req, res, next) => {
  var territory = req.body.territory
  var status = req.body.status
  var remark = req.body.remark
  MonGo.updateCustAppointmentsApproveTerritory(territory, status, remark).then(r => res.json(r))
})
app.post('/approve', (req, res, next) => {
  var empcode = req.body.empcode
  var territory = req.body.territory
  var ownerthor = req.body.ownerthor
  MonGo.GetCustAppointmentsListApprove(empcode, territory, ownerthor).then(r => res.json(r))
})
app.post('/approve/confirm', (req, res, next) => {
  var appointmentcode = req.body.appointmentcode
  MonGo.AppointsCheckStatus(appointmentcode).then(r => res.json(r))
})
app.post('/customer', (req, res, next) => {
  var token = req.body.token
  var empcode = req.body.empcode
  var txtfilter = req.body.txtfilter
  MonGo.AppointsLoadCustomer(token, empcode, txtfilter).then(r => res.json(r))
})

app.post('/new', (req, res, next) => {
  var territory = req.body.territory
  var empcode = req.body.empcode
  var obj = req.body.data
  var approve = req.body.approve
  MonGo.AppointsCustomerAdd(territory, empcode, approve, obj).then(r => res.json(r))
})

export default app
