import Auth from './auth'
import Users from './mock'
import Employees from './employees'
import Master from './master'
import Custtable from './custtable'
import SyncMysqlToMongo from './sync'
import InvenTables from './inventtable'
import Orders from './orders'
import Nottifies from './notifies'
import ResultAppointment from './resultappoint'
import ResultPostpect from './postpect'
import AdjustMent from './adjustments'
import SendMailRouter from './mailer'
import Annote from './annotes'
import Reports from './reports'
import Appointments from './appointment'
import Claim from './claim'

export {
  Auth,
  Users,
  Employees,
  Master,
  Custtable,
  SyncMysqlToMongo,
  InvenTables,
  Orders,
  Nottifies,
  ResultAppointment,
  ResultPostpect,
  AdjustMent,
  SendMailRouter,
  Annote,
  Reports,
  Appointments,
  Claim
}
