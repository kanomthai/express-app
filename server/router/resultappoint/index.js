import { Router } from 'express'
import MonGo from '../../controller/mongo.module'
const app = new Router()
app.post('/', (req, res, next) => {
  MonGo.GetResultAppointment(req.body.empcode, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/result', (req, res, next) => {
  // res.json({data: req.body.data, token: req.headers['x-access-token']})
  MonGo.InsertResultAppointment(req.body.data, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/result/update', (req, res, next) => {
  MonGo.InsertResultAppointmentNewUpdate(req.body.data, req.headers['x-access-token']).then(r => res.json(r))
})
app.put('/', (req, res, next) => {
  MonGo.GetResultAppointment(req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/postpect', (req, res, next) => {
  MonGo.InsertPostPect(req.body.postpect, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/count', (req, res, next) => {
  MonGo.IGetResultAppointmentCount(req.body.empcode).then(r => res.json(r))
})
app.post('/count/add', (req, res, next) => {
  MonGo.IGetResultAppointmentCountAdd(req.body.resultcode, req.body.newdate).then(r => res.json(r))
})
export default app
