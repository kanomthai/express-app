import { Router } from 'express'
import MonGo from '../../controller/mongo.module'
const app = new Router()

app.get('/', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.GetEmployeesList().then(r => res.json(r))
})
app.post('/reset', (req, res, next) => {
  MonGo.ResetEmployeesList(req.body.empid, req.body.pwd).then(r => res.json(r))
})
export default app
