import { Router } from 'express'
import Orders from '../../controller/mongo.module'
const app = new Router()
app.post('/get', (req, res, next) => {
  Orders.GetOrderList(req.body.territory,req.body.position,req.body.empcode,req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/list', (req, res, next) => {
  Orders.GetOrderListById(req.body.empid).then(r => res.json(r))
})
app.post('/search', (req, res, next) => {
  var empid = req.body.empid
  var custname = req.body.custname
  var position = req.body.position
  Orders.GetOrderListByCustomer(empid, custname, position).then(r => res.json(r))
})
app.post('/', (req, res, next) => {
  Orders.InsertOrder(req.body.order, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/update', (req, res, next) => {
  Orders.UpdateOrder(req.body.approveby,req.body.orderid, req.body.status, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/ax', (req, res, next) => {
  var territory = req.body.territory
  var empid = req.body.empid
  var position = req.body.position
  var customer = req.body.customer
  Orders.LoadAxOrder(territory, empid, position, customer).then(r => res.json(r))
})
export default app
