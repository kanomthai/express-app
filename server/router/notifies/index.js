import { Router } from 'express'
import Nottifies from '../../controller/nottifies'

var app = new Router()
app.post('/', function (req, res, next) {
  var message = req.body.message
  Nottifies.GetLineNottifies(message).then(r => res.json(r))
})
app.post('/sticker', function (req, res, next) {
  var message = req.body.message
  var messagenum = req.body.stckerid
  var messageid = req.body.stickernum
  var imageThumbnail = req.body.imageThumbnail
  Nottifies.GetLineNottifiesStricker(message, messagenum, messageid, imageThumbnail).then(r => res.json(r))
})
export default app
