import { Router } from 'express'
import MonGo from '../../controller/mongo.module'
import MySQl from '../../controller/mysql.module'
// import greeter from '../../config/greeter'
const app = new Router()

app.get('/employees', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.GetEmployees().then(r => res.json({data: r, redirecto: null}))
})
app.get('/position', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.GetPosition().then(r => res.json({data: r, redirecto: null}))
})
app.get('/territory', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.GetTerritory().then(r => res.json({data: r, redirecto: null}))
})
app.get('/territory/lead', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.GetTerritorylead().then(r => res.json(r))
})
app.post('/territory', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.InsertTerritory(req.body.data).then(r => res.json({data: r, redirecto: null}))
})
app.post('/employees', (req, res, next) => {
  // const token = req.headers['x-access-token']
  MonGo.InsertEmployees(req.body.data).then(rs => res.json(rs))
})

app.get('/mysql/backorder', (req, res, next) => {
  MySQl.GetBackOrder().then(r => res.json(r))
})

app.get('/province', (req, res, next) => {
  MonGo.GetProvinceWithAmphoes().then(r => res.json(r))
})

app.get('/bank', (req, res, next) => {
  MonGo.LoadBank().then(r => res.json(r))
})

app.post('/billing', (req, res, next) => {
  MonGo.LoadBilling(req.body.custnum.toUpperCase()).then(r => res.json(r))
})
app.post('/state', (req, res, next) => {
  MonGo.LoadBilling(req.body.custnum.toUpperCase()).then(r => res.json(r))
})
app.post('/city', (req, res, next) => {
  MonGo.LoadBilling(req.body.custnum.toUpperCase()).then(r => res.json(r))
})
app.post('/zipcode', (req, res, next) => {
  MonGo.LoadCityZipcode(req.body.zipcode).then(r => res.json(r))
})
app.post('/zipcode/all', (req, res, next) => {
  MonGo.LoadCityZipcodeAll().then(r => res.json(r))
})
app.post('/activities', (req, res, next) => {
  MonGo.LoadMasterActivities().then(r => res.json(r))
})
app.get('/social', (req, res, next) => {
  MonGo.LoadMasterSocial().then(r => res.json(r))
})
export default app
