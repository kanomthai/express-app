import { Router } from 'express'
import PostPect from '../../controller/mongo.module'
const app = new Router()
app.get('/', (req, res, next) => {
  PostPect.GetPostPect(req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/find', (req, res, next) => {
  PostPect.PostPectFilter(req.body.filter, req.headers['x-access-token']).then(r => res.json(r))
})
export default app
