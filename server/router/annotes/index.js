import MonGoDB from '../../controller/mongo.module'
const exp = require('express')
const app = exp.Router()
app.post('/', (req, res, next) => {
  const token = req.headers['x-access-token']
  MonGoDB.InsAnnote(req.body.txtid, req.body.data, req.body.title, req.body.type, req.body.doctype, req.body.recid, token).then(r => res.json(r))
})
app.post('/new/account', (req, res, next) => {
  const token = req.headers['x-access-token']
  MonGoDB.InsAnnoteNewAccount(req.body.txtid, req.body.data, req.body.type, token).then(r => res.json(r))
  // MonGoDB.InsAnnoteNewAccount(req.body.txtid, req.body.data, req.body.title, req.body.type, req.body.doctype, req.body.recid, token).then(r => res.json(r))
})
app.post('/del/new/account', (req, res, next) => {
  const token = req.headers['x-access-token']
  MonGoDB.AnnoteDelNewAccount(req.body.recid, token).then(r => res.json(r))
})
app.post('/add', (req, res, next) => {
  const token = req.headers['x-access-token']
  MonGoDB.InsAnnoteAdd(req.body.data, token).then(r => res.json(r))
})
app.post('/del', (req, res, next) => {
  const token = req.headers['x-access-token']
  MonGoDB.AnnoteDel(req.body.recid, token).then(r => res.json(r))
})
app.post('/market', (req, res, next) => {
  const token = req.headers['x-access-token']
  MonGoDB.InsAnnoteMarket(req.body.txtid, req.body.img, req.body.title, req.body.type, req.body.doctype, req.body.refid, req.body.recid, token).then(r => res.json(r))
})
app.post('/img/new', (req, res, next) => {
  MonGoDB.InsAnnotePicNewAccount(req.body.refid, req.body.type).then(r => res.json(r))
})
app.post('/img', (req, res, next) => {
  MonGoDB.InsAnnotePic(req.body.refid, req.body.type).then(r => res.json(r))
})

export default app
