import { Router } from 'express'
import fnSend from '../../controller/sendmail.controller'
const app = new Router()
app.post('/', (req, res, next) => {
  var token = req.headers['x-access-token']
  fnSend.SendMailTo(req.body.data, token).then(r => res.json(r))
})
export default app
