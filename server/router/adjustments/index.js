import { Router } from 'express'
import MonGo from '../../controller/mongo.module'

const app = new Router()

app.post('/credit', (req, res, next) => {
  MonGo.InsAdjustment(req.body.data, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/add/credit', (req, res, next) => {
  MonGo.InsAdjustmentNew(req.body.data, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/rename', (req, res, next) => {
  MonGo.InsAdjustmentRename(req.body.data, req.body.img, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/transport', (req, res, next) => {
  MonGo.InsAdjustmentTransport(req.body.data, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/address', (req, res, next) => {
  MonGo.InsAdjustmentAddress(req.body.data, req.body.img, req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/credit/approve', (req, res, next) => {
  MonGo.ApproveAdjustmentCredit(req.body.empcode, req.body.code, req.body.appr, req.body.remark, req.headers['x-access-token']).then(r => res.json(r))
})
app.get('/credit', (req, res, next) => {
  MonGo.getAdjustmentList(req.headers['x-access-token']).then(r => res.json(r))
})
app.post('/load', (req, res, next) => {
  MonGo.getAdjustmentListTerminal(req.body.token).then(r => res.json(r))
})
app.post('/all', (req, res, next) => {
  MonGo.getAdjustmentList(req.headers['x-access-token'], req.body.customer).then(r => res.json(r))
})
app.post('/social', (req, res, next) => {
  MonGo.InsAdjustmentSocial(req.body.data).then(r => res.json(r))
})

export default app
