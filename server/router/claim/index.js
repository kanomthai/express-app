import { Router } from 'express'
import Claim from '../../controller/mongo.module'
const app = new Router()
app.post('/', (req, res, next) => {
  Claim.GetClaimList(req.body.empcode, req.body.territory, req.body.customer).then(r => res.json(r))
})
// app.post('/list', (req, res, next) => {
//   Claim.GetOrderListById(req.body.empid).then(r => res.json(r))
// })
// app.post('/search', (req, res, next) => {
//   var empid = req.body.empid
//   var custname = req.body.custname
//   var position = req.body.position
//   Claim.GetOrderListByCustomer(empid, custname, position).then(r => res.json(r))
// })
// app.post('/', (req, res, next) => {
//   Claim.InsertOrder(req.body.order, req.headers['x-access-token']).then(r => res.json(r))
// })
// app.post('/update', (req, res, next) => {
//   Claim.UpdateOrder(req.body.orderid, req.body.status, req.headers['x-access-token']).then(r => res.json(r))
// })
// app.post('/ax', (req, res, next) => {
//   var territory = req.body.territory
//   var empid = req.body.empid
//   var position = req.body.position
//   var customer = req.body.customer
//   Claim.LoadAxOrder(territory, empid, position, customer).then(r => res.json(r))
// })
export default app
