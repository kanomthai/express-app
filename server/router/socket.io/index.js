import { Router } from 'express'
import Mysql from '../../controller/mysql.module'
import MonGo from '../../controller/mongo.module'
// const ex = new Express()
// var server = require('http').createServer(ex)
// var io = require('socket.io')(server)

const app = new Router()
app.get('/', (req, res, next) => {
  res.json({data: 'load ok'})
})
app.get('/backorder', (req, res, next) => {
  Mysql.GetBackOrder().then(r => res.json(r))
})
app.get('/custtable', (req, res, next) => {
  Mysql.GetCusttable().then(r => res.json(r))
})
app.get('/address', (req, res, next) => {
  Mysql.GetCustAddress().then(r => res.json(r))
})
app.get('/custtrans', (req, res, next) => {
  Mysql.GetCusttrans().then(r => res.json(r))
})
app.get('/balancecredit', (req, res, next) => {
  Mysql.GetCustBalanceCredit().then(r => res.json(r))
})
app.get('/inventtable', (req, res, next) => {
  Mysql.GetInventtable().then(r => res.json(r))
})
app.get('/inventtable/price', (req, res, next) => {
  Mysql.GetInventtablePrice().then(r => res.json(r))
})

app.get('/custdist', (req, res, next) => {
  Mysql.GetCustCreditDisc().then(r => res.json(r))
})

app.get('/salesorder', (req, res, next) => {
  MonGo.CRMOrderToCloud().then(rs => res.json(rs))
})
export default app
