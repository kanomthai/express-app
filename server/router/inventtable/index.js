import { Router } from 'express'
import MonGo from '../../controller/mongo.module'
const app = new Router()
app.post('/', (req, res, next) => {
  var items = req.body.item
  if (items !== null) {
    items = items.toUpperCase()
  }
  MonGo.GetInventTable(items, req.body.custnum.toUpperCase(), req.body.groupid.toUpperCase()).then(r => res.json(r))
})
app.get('/', (req, res, next) => {
  MonGo.GetInventTableList().then(r => res.json(r))
})
app.post('/list', (req, res, next) => {
  var items = req.body.item
  if (items !== null && items.length > 0) {
    items = items.toUpperCase()
  }
  MonGo.GetInventTableByName(items).then(r => res.json(r))
})
app.post('/etc', (req, res, next) => {
  var items = req.body.item
  if (items !== null && items.length > 0) {
    items = items.toUpperCase()
  }
  MonGo.GetInventTableByNameETC(items, req.body.ordertype).then(r => res.json(r))
})
app.post('/spc', (req, res, next) => {
  var items = req.body.item
  if (items !== null && items.length > 0) {
    items = items.toUpperCase()
  }
  MonGo.GetInventTableByNameSPC(items, req.body.typeitem).then(r => res.json(r))
})
app.post('/spclist', (req, res, next) => {
  var items = req.body.item
  if (items !== null && items.length > 0) {
    items = items.toUpperCase()
  }
  MonGo.GetInventTableByNameSPCList(items).then(r => res.json(r))
})
app.post('/sync/new', (req, res, next) => {
  var e = {
    recid: req.body.recid,
    itemid: req.body.itemid,
    itemname: req.body.itemname,
    datestockin: req.body.datestockin,
    stockstatus: req.body.stockstatus,
    amount: req.body.amount,
    stockstatusclass: req.body.stockstatusclass,
    unitid: req.body.unitid,
    stop: req.body.stop,
    datestock: req.body.datestock,
    quantityamount: req.body.quantityamount,
    salestype: req.body.salestype,
    salesremark: req.body.salesremark
  }
  MonGo.InsertNewInventory(e).then(r => res.json(r))
})
export default app
