import { Router } from 'express'
import MonGo from '../../controller/mongo.module'

const app = new Router()
app.get('/data', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetCusttableTerritory(token).then(r => res.json(r))
})
app.post('/data', (req, res, next) => {
  MonGo.GetRegCusttableTerritoryList(req.body.custnum, req.body.territory, req.body.territoryid).then(r => res.json(r))
})
app.get('/data/territory', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetCusttableTerritoryList(token).then(r => res.json(r))
})

app.get('/', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetNewListCustomer(token).then(r => res.json(r))
})

app.get('/reports', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.getViewReports(token).then(r => res.json(r))
})
app.post('/img', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadImgAnnote(req.body.txtid, token).then(r => res.json(r))
})
app.post('/find/billing', (req, res, next) => {
  MonGo.GetRegCusttableBilling(req.body.territory).then(r => res.json(r))
})
app.post('/find', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetRegCusttable(req.body.custnum, token).then(r => res.json(r))
})

app.post('/find/adjust', (req, res, next) => {
  MonGo.GetRegCusttableTerritoryList(req.body.custnum, req.body.territory, req.body.territoryid).then(r => res.json(r))
})

app.post('/find/territory', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetRegCusttableTerritory(req.body.territory, token).then(r => res.json(r))
})

app.post('/tricredit', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetRegCusttable(req.body.custnum.toUpperCase(), token).then(r => res.json(r))
})

app.post('/backorder', (req, res, next) => {
  res.json({data: 'ok backorder'})
})
app.post('/status', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpsCustomer(req.body.txtid, req.body.comment, req.body.code, token, req.body.empcode).then(r => res.json(r))
})
app.post('/txtid/check', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.CheckCustomerTxtIdNull(req.body.txtid, req.body.territory, token).then(r => res.json(r))
})

app.post('/txtid', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.CheckCustomerTxtId(req.body.txtid, req.body.territory, token).then(r => res.json(r))
})
app.post('/update', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.GetNewCustomer(req.body.txtid, token, req.body.data).then(r => res.json(r))
})
app.post('/check/address', (req, res, next) => {
  MonGo.CheckNewCustomerAddress(req.body.txtid).then(r => res.json(r))
})
app.post('/check/annote', (req, res, next) => {
  MonGo.CheckNewCustomerAnnote(req.body.txtid).then(r => res.json(r))
})
app.post('/address', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.InsCustomerAddress(req.body.txtid, token, req.body.data, req.body.addresstype).then(r => res.json(r))
})
app.post('/address/load', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadAddress(req.body.txtid, token).then(r => res.json(r))
})
app.post('/address/update', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpdsCustomerAddress(req.body.txtid, token, req.body.data, req.body.addresscode).then(r => res.json(r))
})
app.post('/address/delete', (req, res, next) => {
  MonGo.InsCustomerAddressDelete(req.body.addresscode).then(r => res.json(r))
})
app.post('/meeting', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadMeetingCustomer(req.body.txtid, token).then(r => res.json(r))
})
app.post('/meeting/update', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpdateMeetingCustomer(req.body.txtid, req.body.data, token).then(r => res.json(r))
})
app.post('/transport', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpdateTransportCustomer(req.body.txtid, req.body.data, token).then(r => res.json(r))
})
app.post('/transport/add', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpdateTransportCustomerNew(req.body.txtid, req.body.data, token).then(r => res.json(r))
})
app.post('/social/del', (req, res, next) => {
  // var token = req.headers['x-access-token']
  MonGo.DelSocialMediaCustomerNew(req.body.socode).then(r => res.json(r))
})
app.post('/social/add', (req, res, next) => {
  // var token = req.headers['x-access-token']
  MonGo.UpdateSocialMediaCustomerNew(req.body.txtid, req.body.obj).then(r => res.json(r))
})
app.post('/social/load', (req, res, next) => {
  // var token = req.headers['x-access-token']
  MonGo.LoadSocialMediaCustomerNew(req.body.txtid).then(r => res.json(r))
})
app.post('/market/load', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadMarketDataCustomer(req.body.txtid, req.body.data, token).then(r => res.json(r))
})

app.post('/market', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpdateMarketDataCustomer(req.body.txtid, req.body.data, token).then(r => res.json(r))
})
app.post('/marketshr/load', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadMarketShr(req.body.txtid, token).then(r => res.json(r))
})
app.post('/marketshr/add', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.InsertMarketShr(req.body.txtid, req.body.data, req.body.code, token).then(r => res.json(r))
})

app.post('/credit', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.UpdateCustCredit(req.body.txtid, req.body.data, token).then(r => res.json(r))
})

app.post('/credit/load', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadCustCredit(req.body.txtid, token).then(r => res.json(r))
})

app.post('/contact/load', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.LoadContact(req.body.txtid, token).then(r => res.json(r))
})
app.post('/contact/add', (req, res, next) => {
  var token = req.headers['x-access-token']
  MonGo.insertContact(req.body.txtid, req.body.data, req.body.gcode, req.body.pathname, req.body.img, token).then(r => res.json(r))
})
app.post('/phone/add', (req, res, next) => {
  MonGo.insertPhone(req.body.custnum, req.body.custname, req.body.telnumberold, req.body.telnumbernew, req.body.remark).then(r => res.json(r))
})

export default app
