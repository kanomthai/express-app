/* jshint node: true */
import fnUser from '../module/users/users.module'
import Employees from '../module/employees'
import fCust from '../module/custtable'
import fInventtable from '../module/inventtable'
import fOrder from '../module/orders'
import fMaster from '../module/master'
import fResultAppoint from '../module/resultappointment'
import fPostPect from '../module/postpect'
import fSaleOrder from '../module/mongotomysql'
import fAdjustment from '../module/adjustments'
import fAnnote from '../module/annotes'
import gView from '../module/reports'
import Appoints from '../module/appointment'
import Claim from '../module/claim'
const fnKey = require('../../assets/js/line.key')

export default {
  InsertNewInventory: async e => {
    var data = await fInventtable.InsertNewInventory(e)
    return data
  },
  LoadResultAppointmentActivitiesMarket: async emp => {
    var data = await gView.LoadResultAppointmentActivitiesMarket(emp)
    return data
  },
  LoadResultAppointmentActivities: async (empcode) => {
    var data = gView.LoadResultAppointmentActivities(empcode)
    return data
  },
  LoadMasterActivities: async () => {
    var data = await fMaster.LoadMasterActivities()
    return data
  },
  CheckUsers: async (email, password) => {
    // var e = {
    //   email: email, 
    //   key: fnKey.lineKeyTok(password)
    // }
    // return e
    var data = await fnUser.CheckUsers(email, fnKey.lineKeyTok(password))
    return data
  },
  CheckUsersPwd: async (email, password) => {
    var data = await fnUser.CheckUsersPwd(email, fnKey.lineKeyTok(password))
    return data
  },
  ResetEmployeesList: async (empid, password) => {
    var data = await Employees.ResetEmployeesList(empid, fnKey.lineKeyTok(password))
    return data
  },
  GetCusttableTerritory: async token => {
    var data = await fCust.GetCusttableTerritory(token)
    return data
  },
  GetCusttableTerritoryList: async token => {
    var data = await fCust.GetCusttableTerritoryList(token)
    return data
  },
  GetNewListCustomer: async token => {
    var data = await fCust.GetNewCustomer(token)
    return data
  },
  GetEmployeesList: async () => {
    var data = await Employees.GetEmployeesList()
    return data
  },
  GetEmployees: async () => {
    var data = await Employees.GetEmployees()
    return data
  },
  GetPosition: async () => {
    var data = await Employees.GetPosition()
    return data
  },
  GetTerritory: async () => {
    var data = await Employees.GetTerritory()
    return data
  },
  GetTerritorylead: async () => {
    var data = await Employees.GetTerritoryLead()
    return data
  },
  InsertEmployees: async data => {
    var d = await Employees.InsertEmployees(data)
    return d
  },
  InsertTerritory: async data => {
    var d = await Employees.InsertTerritory(data)
    return d
  },
  GetRegCusttable: async (custnum, token) => {
    var data = await fCust.GetRegCusttable(custnum, token)
    return data
  },
  GetRegCusttableTerritory: async (territory, token) => {
    var data = await fCust.GetRegCusttableTerritory(territory, token)
    return data
  },
  GetInventTable: async (filter, custnum, groupid) => {
    var data = await fInventtable.GetInventTable(filter, custnum, groupid)
    return data
  },
  GetInventTableList: async () => {
    var data = await fInventtable.GetInventTableList()
    return data
  },
  GetInventTableByName: async (items) => {
    var data = await fInventtable.GetInventTableByNam(items)
    return data
  },
  GetOrderList: async (territory,position,empcode,token) => {
    var data = await fOrder.GetOrders(territory,position,empcode,token)
    return data
  },
  GetOrderListByCustomer: async (empid, custname, position) => {
    var data = await fOrder.GetOrderListByCustomer(empid, custname, position)
    return data
  },
  GetOrderListById: async empid => {
    var data = await fOrder.GetOrderListById(empid)
    return data
  },
  InsertOrder: async (body, token) => {
    var data = await fOrder.InsertOrder(body, token)
    return data
  },
  UpdateOrder: async (approveby,orderid,status,token) => {
    var data = await fOrder.UpdateOrder(approveby,orderid,status,token)
    return data
  },
  GetProvinceWithAmphoes: async () => {
    var data = await fMaster.GetProvince()
    return data
  },
  InsertPostPect: async (postpect, token) => {
    var data = await fResultAppoint.InsertPostPect(postpect, token)
    return data
  },
  GetResultAppointment: async (empcode, token) => {
    var data = await fResultAppoint.LoadResultAppointment(empcode, token)
    return data
  },
  InsertResultAppointment: async (appoint, token) => {
    var data = await fResultAppoint.InsertResultAppointment(appoint, token)
    return data
  },
  InsertResultAppointmentNewUpdate: async (appoint, token) => {
    var data = await fResultAppoint.InsertResultAppointmentNewUpdate(appoint, token)
    return data
  },
  GetPostPect: async token => {
    var data = await fPostPect.GetVisitPostPect(token)
    return data
  },
  PostPectFilter: async (filter, token) => {
    var data = await fPostPect.GetPostPect(filter, token)
    return data
  },
  CRMOrderToCloud: async () => {
    var data = await fSaleOrder.GetOrder()
    return data
  },
  LoadBank: async () => {
    var data = await fMaster.LoadBank()
    return data
  },
  LoadBilling: async custnumber => {
    var data = await fMaster.LoadBilling(custnumber)
    return data
  },
  LoadAxOrder: async (territory, empid, position, customer) => {
    var data = await fOrder.GetOrdersAxOrder(territory, empid, position, customer)
    return data
  },
  InsAdjustment: async (forms, token) => {
    var data = await fAdjustment.InsAdjustCredit(forms, token)
    return data
  },
  InsAdjustmentRename: async (forms, img, token) => {
    var data = await fAdjustment.InsAdjustRename(forms, img, token)
    return data
  },
  InsAdjustmentTransport: async (forms, token) => {
    var data = await fAdjustment.InsAdjustTransport(forms, token)
    return data
  },
  InsAdjustmentAddress: async (forms, img, token) => {
    var data = await fAdjustment.InsAdjustAddress(forms, img, token)
    return data
  },
  getAdjustmentListTerminal: async token => {
    var data = await fAdjustment.getAdjustmentListTerminal(token)
    return data
  },
  getAdjustmentList: async token => {
    var data = await fAdjustment.getAdjustmentList(token)
    return data
  },
  getAdjustmentListfilter: async (token, customer) => {
    var data = await fAdjustment.getAdjustmentListfilter(token, customer)
    return data
  },
  ApproveAdjustmentCredit: async (empcode, code, appr, remark) => {
    var data = await fAdjustment.UpdateAdjustMentCredit(empcode, code, appr, remark)
    return data
  },
  CheckCustomerTxtIdNull: async (txtid, token) => {
    var data = await fCust.CheckCustomerTxtIdNull(txtid, token)
    return data
  },
  CheckCustomerTxtId: async (txtid, territory, token) => {
    var data = await fCust.GetCheckTxtId(txtid, territory, token)
    return data
  },
  GetNewCustomer: async (txtid, token, Obj) => {
    var data = await fCust.GetCusttablesTxtId(txtid, token, Obj)
    return data
  },
  InsAnnote: async (txtid, obj, title, type, doctype, recid, token) => {
    var data = await fAnnote.InsAnnote(txtid, obj, title, type, doctype, recid, token)
    return data
  },
  InsAnnoteNewAccount: async (txtid, obj, title, type, doctype, recid, token) => {
    var data = await fAnnote.InsAnnoteNewAccount(txtid, obj, title, type, doctype, recid, token)
    return data
  },
  InsAnnoteAdd: async (Obj, token) => {
    var data = await fAnnote.InsAnnoteAdd(Obj, token)
    return data
  },
  InsCustomerAddress: async (txtid, token, Obj, addresstype) => {
    var data = await fCust.GetAddressTable(txtid, token, Obj, addresstype)
    return data
  },
  UpdsCustomerAddress: async (txtid, token, Obj, addresscode) => {
    var data = await fCust.GetUpdateAddress(txtid, token, Obj, addresscode)
    return data
  },
  InsCustomerAddressDelete: async addresscode => {
    var data = await fCust.InsCustomerAddressDelete(addresscode)
    return data
  },
  LoadAddress: async (txtid, token) => {
    var data = await fCust.GetLoadAddress(txtid, token)
    return data
  },
  AnnoteDelNewAccount: async (recid, token) => {
    var data = await fAnnote.AnnoteDelNewAccount(recid, token)
    return data
  },
  AnnoteDel: async (recid, token) => {
    var data = await fAnnote.DelAnnote(recid, token)
    return data
  },
  LoadMeetingCustomer: async (txtid, token) => {
    var data = fCust.GetMeetingNewCust(txtid, token)
    return data
  },
  UpdateMeetingCustomer: async (txtid, Obj, token) => {
    var data = fCust.UpdateMeetingNewCust(txtid, Obj, token)
    return data
  },
  UpdateTransportCustomerNew: async (txtid, Obj, token) => {
    var data = fCust.UpdateTransportCustomerNew(txtid, Obj, token)
    return data
  },
  UpdateTransportCustomer: async (txtid, Obj, token) => {
    var data = fCust.UpdateTransportCustomer(txtid, Obj, token)
    return data
  },
  LoadMarketDataCustomer: async (txtid, Obj, token) => {
    var data = fCust.LoadMarketDataCustomer(txtid, Obj, token)
    return data
  },
  UpdateMarketDataCustomer: async (txtid, Obj, token) => {
    var data = fCust.UpdateMarketDataCustomer(txtid, Obj, token)
    return data
  },
  InsAnnoteMarket: async (txtid, img, title, type, doctype, refid, recid, token) => {
    var data = await fAnnote.InsAnnoteMarket(txtid, img, title, type, doctype, refid, recid, token)
    return data
  },
  InsAnnotePic: async (refid, type) => {
    var data = await fAnnote.InsAnnotePic(refid, type)
    return data
  },
  InsAnnotePicNewAccount: async (refid, type) => {
    var data = await fAnnote.InsAnnotePicNewAccount(refid, type)
    return data
  },
  LoadCustCredit: async (txtid, token) => {
    var data = await fCust.LoadCustCredit(txtid, token)
    return data
  },
  UpdateCustCredit: async (txtid, Obj, token) => {
    var data = await fCust.UpdateCustCredit(txtid, Obj, token)
    return data
  },
  LoadContact: async (txtid, token) => {
    var data = await fCust.LoadContact(txtid, token)
    return data
  },
  insertContact: async (txtid, Obj, gCode, pathname, img, token) => {
    var data = await fCust.insertContact(txtid, Obj, gCode, pathname, img, token)
    return data
  },
  LoadMarketShr: async (txtid, token) => {
    var data = await fCust.LoadMarketShr(txtid, token)
    return data
  },
  InsertMarketShr: async (txtid, Obj, code, token) => {
    var data = await fCust.InsertMarketShr(txtid, Obj, code, token)
    return data
  },
  LoadImgAnnote: async (txtid, token) => {
    var data = await fCust.LoadImgAnnote(txtid, token)
    return data
  },
  UpsCustomer: async (txtid, Comment, St, token, empcode) => {
    var data = await fCust.UpsCustomer(txtid, Comment, St, token, empcode)
    return data
  },
  getViewReports: async token => {
    var data = await gView.GetViewTarget(token)
    return data
  },
  AppointsGetData: async (empcode, territory) => {
    var data = await Appoints.GetCustAppointments(empcode, territory)
    return data
  },
  AppointsInsData: async (empcode, territory, Obj) => {
    var data = await Appoints.InsAppointments(empcode, territory, Obj)
    return data
  },
  AppointsCheckStatus: async appointmentcode => {
    var data = await Appoints.AppointsCheckStatus(appointmentcode)
    return data
  },
  AppointsLoad: async territory => {
    var data = await Appoints.AppointsLoad(territory)
    return data
  },
  AppointsLoadNew: async territory => {
    var data = await Appoints.AppointsLoadNew(territory)
    return data
  },
  GroupAppointments: async territory => {
    var data = await Appoints.GroupAppointments(territory)
    return data
  },
  GroupCountAppointments: async territory => {
    var data = await Appoints.GroupCountAppointments(territory)
    return data
  },
  GetRegCusttableBilling: async territory => {
    var data = await fCust.GetRegCusttableBilling(territory)
    return data
  },
  GetCustAppointmentsListApprove: async (empcode, territory, ownerthor) => {
    var data = await Appoints.GetCustAppointmentsListApprove(empcode, territory, ownerthor)
    return data
  },
  AppointsCopy: async (appointmentcode, territory, empcode) => {
    var data = await Appoints.AppointsCopy(appointmentcode, territory, empcode)
    return data
  },
  AppointsApprove: async (appointmentcode, status, remark) => {
    var data = await Appoints.AppointsApprove(appointmentcode, status, remark)
    return data
  },
  updateCustAppointmentsApproveTerritory: async (territory, status, remark) => {
    var data = await Appoints.updateCustAppointmentsApproveTerritory(territory, status, remark)
    return data
  },
  GetAppointsApprove: async (territory, status) => {
    var data = await Appoints.GetAppointsApprove(territory, status)
    return data
  },
  LoadCityZipcode: async zipcode => {
    var data = await fMaster.LoadCityZipcode(zipcode)
    return data
  },
  LoadCityZipcodeAll: async () => {
    var data = await fMaster.LoadCityZipcodeAll()
    return data
  },
  CheckNewCustomerAddress: async txtid => {
    var data = await fCust.CheckNewCustomerAddress(txtid)
    return data
  },
  CheckNewCustomerAnnote: async txtid => {
    var data = await fCust.CheckNewCustomerAnnote(txtid)
    return data
  },
  LoadResultAppointmentCollection: async empcode => {
    var data = await fResultAppoint.LoadResultAppointmentCollection(empcode)
    return data
  },
  LoadResultAppointmentCollectionUpdate: async (resultcode, Obj) => {
    var data = await fResultAppoint.LoadResultAppointmentCollectionUpdate(resultcode, Obj)
    return data
  },
  insertPhone: async (custnum, custname, telnumberold, telnumbernew, remark) => {
    var data = await fAdjustment.insertPhone(custnum, custname, telnumberold, telnumbernew, remark)
    return data
  },
  GetRegCusttableTerritoryList: async (custnum, territory, territoryid) => {
    var data = await fCust.GetRegCusttableTerritoryList(custnum, territory, territoryid)
    return data
  },
  IGetResultAppointmentCount: async empcode => {
    var data = await fResultAppoint.IGetResultAppointmentCount(empcode)
    return data
  },
  IGetResultAppointmentCountAdd: async (resultcode, newdate) => {
    var data = await fResultAppoint.IGetResultAppointmentCountAdd(resultcode, newdate)
    return data
  },
  InsAdjustmentNew: async Obj => {
    var data = await fAdjustment.InsAdjustmentNew(Obj)
    return data
  },
  LoadMasterSocial: async () => {
    var data = await fMaster.LoadMasterSocial()
    return data
  },
  UpdateSocialMediaCustomerNew: async (txtid, obj) => {
    var data = await fCust.UpdateSocialMediaCustomerNew(txtid, obj)
    return data
  },
  LoadSocialMediaCustomerNew: async txtid => {
    var data = await fCust.LoadSocialMediaCustomerNew(txtid)
    return data
  },
  DelSocialMediaCustomerNew: async socode => {
    var data = await fCust.DelSocialMediaCustomerNew(socode)
    return data
  },
  InsAdjustmentSocial: async obj => {
    var data = await fAdjustment.InsAdjustmentSocial(obj)
    return data
  },
  GetRegCusttableTerritoryAppointments: async (territory, empcode) => {
    var data = await Appoints.GetRegCusttableTerritoryAppointments(territory, empcode)
    return data
  },
  GetInventTableByNameSPC: async (itemsname, typeitem) => {
    var data = await fInventtable.GetInventTableByNameSPC(itemsname, typeitem)
    return data
  },
  GetInventTableByNameSPCList: async (itemsname) => {
    var data = await fInventtable.GetInventTableByNameSPCList(itemsname)
    return data
  },
  GetInventTableByNameETC: async (items, ordertype) => {
    var data = await fInventtable.GetInventTableByNameETC(items, ordertype)
    return data
  },
  // Get Claim
  GetClaimList: async (empcode, territory, customer) => {
    var data = await Claim.GetClaimList(empcode, territory, customer)
    return data
  },
  AppointsLoadCustomer: async (token, empcode) => {
    var data = await Appoints.AppointsLoadCustomer(token, empcode)
    return data
  },
  AppointsCustomerAdd: async (territory, empcode, approve, data) => {
    var obj = await Appoints.AppointsCustomerAdd(territory, empcode, approve, data)
    return obj
  }

}
