import Greeter from '../config/greeter'
export default {
  SendMailTo: async (mForm, token) => {
    var data = await Greeter.SendMailNodeJS(mForm, token)
    return data
  }
}
