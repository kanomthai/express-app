import Nottifies from '../module/notifies'

export default {
  GetLineNottifies: async msg => {
    var data = await Nottifies.LineNottifies(msg)
    return data
  },
  GetLineNottifiesStricker: async (msg, sid, snum, imageThumbnail) => {
    var data = await Nottifies.LineNottifiesSticker(msg, sid, snum, imageThumbnail)
    return data
  }
}
