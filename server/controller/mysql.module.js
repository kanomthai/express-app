import fnMysql from '../module/mysqltomongo/backorder.module'
import fnMaster from '../module/master/backorder.mysql'
import fnUsers from '../module/users/users.module'
export default {
  GetBackOrder: async () => {
    var data = await fnMysql.GetBackOrder()
    return data
  },
  GetCusttable: async () => {
    var data = await fnMysql.GetCusttable()
    return data
  },
  GetCusttrans: async () => {
    var data = await fnMysql.GetCusttrans()
    return data
  },
  GetCustBalanceCredit: async () => {
    var data = await fnMysql.GetCustBalanceCredit()
    return data
  },
  GetInventtable: async () => {
    var data = await fnMysql.GetInventtable()
    return data
  },
  GetInventtablePrice: async () => {
    var data = await fnMysql.GetInventtablePrice()
    return data
  },
  GetCustAddress: async () => {
    var data = await fnMysql.GetCustAddress()
    return data
  },
  GetCustCreditDisc: async () => {
    var data = await fnMysql.GetCustCreditDisc()
    return data
  },
  SyncUsersToMonGo: async () => {
    var data = await fnUsers.SyncUsersToMonGoDB()
    return data
  },
  GetBilling: async () => {
    var data = await fnMaster.GetBilling()
    return data
  }
}
