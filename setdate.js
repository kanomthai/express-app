var db = 'mongodb://192.168.2.1:27017/cloud'
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')

function guid () {
  function s4 () {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1)
  }
  return (s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4()).toUpperCase()
}

var GenCode = () => {
  var g = guid()
  return g.toUpperCase()
}

var setdate = () => {
  return new Promise(resolve => {
    MongoClient.connect(db, (err, db) => {
      assert.equal(err, null)
      db.collection('appointment').find({empcode: 'SNE1162'})
      // db.collection('appointment').find()
        .toArray((er, res) => {
          assert.equal(err, null)
          var data = res
          data.forEach((e, i) => {
            var cdate = GenCode()
            console.log('code: ' + e.appointid + ' create:' + cdate)
            db.collection('appointment').update({
              appointid: e.appointid
            }, {$set: {
              territory: 'D05',
              empteritory: 'D05',
              appointmentcode: cdate,
              appointid: GenCode(),
              syncstatus: 0
            }})
          })
          resolve()
        })
    })
  })
}
setdate()

// "visitordersdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "create" : ISODate("2018-03-08T02:28:55.986Z"),
//     "created_at" : ISODate("2018-03-08T01:45:19.945Z"),
//     "transdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitcollectiondate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "collectiondate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitbillingdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitactivitiesdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitconditionsdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitcompetitorsdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "visitpostpectdate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "moneydate" : ISODate("2018-03-08T02:28:55.986Z"),
//     "update" : ISODate("2018-03-08T02:28:55.986Z"),
//     "created" : ISODate("2018-03-08T02:28:55.986Z")
