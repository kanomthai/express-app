const firebaseConfig = {
  apiKey: 'AIzaSyCEgPnuWk5IQjVWNbrkUKNDiU6lZEYss10',
  authDomain: 'grounded-burner-199606.firebaseapp.com',
  databaseURL: 'https://grounded-burner-199606.firebaseio.com',
  projectId: 'grounded-burner-199606',
  storageBucket: 'grounded-burner-199606.appspot.com',
  messagingSenderId: '1072089269744'
}
const firebaseConfigTest = {
  apiKey: 'AIzaSyCh2H8byrcAZpA6KxKL89S_5VFdqj2zj-M',
  authDomain: 'test-app-da8c9.firebaseapp.com',
  databaseURL: 'https://test-app-da8c9.firebaseio.com',
  projectId: 'test-app-da8c9',
  storageBucket: 'test-app-da8c9.appspot.com',
  messagingSenderId: '338313947212'
}
export {
  firebaseConfig,
  firebaseConfigTest
}
