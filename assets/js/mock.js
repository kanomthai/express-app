export default {
  listcustomertype: [
    {id: 0, title: 'เลือกประเภทสำนักงาน', value: 0},
    {id: 0, title: 'สำนักงานใหญ่', value: 1},
    {id: 1, title: 'สำนักงานย่อย', value: 2}
  ],
  customerclosetype: [
    {id: 0, title: 'เปิดการขาย', value: 0, status: true},
    {id: 1, title: 'ระงับการขาย', value: 1, status: true}
  ],
  customerclose: [
    {id: 0, title: 'เลิกกิจการ', value: 0, status: true},
    {id: 1, title: 'ปัญหาด้านการเงิน', value: 1, status: true},
    {id: 2, title: 'Clear Doc.', value: 2, status: true}
  ],
  customertype: [
    {id: 0, title: 'นิติบุคคล / Corporate', value: 1},
    {id: 1, title: 'บุคคลธรรมดา / Individual', value: 2}
  ],
  employeestype: [
    {id: 0, title: 'ไม่ระบุ / None'},
    {id: 1, title: 'เจ้าของร้าน / Manager'},
    {id: 2, title: 'พนักงานจัดซื้อ / Purchasing Officer'},
    {id: 3, title: 'พนักงานบัญชี / Accountant'}
  ],
  addresslist: [
    {id: 0, title: 'เลือกประเภทที่อยู่', value: 0},
    {id: 1, title: 'ที่อยู่สำหรับออกใบกำกับภาษี / Invoice', value: 1},
    {id: 2, title: 'ที่อยู่สำหรับส่งสินค้า / Delivery', value: 2},
    {id: 3, title: 'ที่อยู่ร้านหรือเจ้าของร้าน / Owner', value: 3}
  ],
  datenormal: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  date: [
    {id: 0, title: 'วันอาทิตย์ / Sunday', checked: true},
    {id: 1, title: 'วันจันทร์ / Monday', checked: true},
    {id: 2, title: 'วันอังคาร / Tuesday', checked: true},
    {id: 3, title: 'วันพุธ / Wednesday', checked: true},
    {id: 4, title: 'วันพฤหัส / Thursday', checked: true},
    {id: 5, title: 'วันศุกร์ / Friday', checked: true},
    {id: 6, title: 'วันเสาร์ / Saturday', checked: true}
  ],
  activitieslist: [
    {id: 1, title: 'ตกแต่งหน้าร้าน', status: true},
    {id: 2, title: 'แจกของ Premium', status: false},
    {id: 3, title: 'Update สินค้าใหม่', status: true},
    {id: 4, title: 'แจกโปรชัวร์', status: false}
  ],
  datespec: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  masterbusiness: [
    {id: 0, title: 'นิติบุคคล'},
    {id: 1, title: 'บุคคลธรรมดา'}
  ],
  mastercredit: [
    {id: 0, title: 'ไม่ระบุ / None', description: 'ไม่ระบุ', creditval: true, amount: 0},
    {id: 1, title: 'SD-CASH', description: 'CASH', creditval: false, amount: 0},
    {id: 2, title: 'SD-CR07', description: 'CREDIT 7 DAYS', creditval: true, amount: 1},
    {id: 3, title: 'SD-CR15', description: 'CREDIT 15 DAYS', creditval: true, amount: 1},
    {id: 4, title: 'SD-CR30', description: 'CREDIT 30 DAYS', creditval: true, amount: 1},
    {id: 5, title: 'SD-CR60', description: 'CREDIT 60 DAYS', creditval: true, amount: 1},
    {id: 6, title: 'SD-CR60AF', description: '60 DAYS AFTER DELIVERY DATE', creditval: true, amount: 1},
    {id: 7, title: 'SD-CR90', description: 'CREDIT 90 DAYS', creditval: true, amount: 1}
  ],
  mastertypecash: [
    {id: 0, title: 'อื่นๆ / Other'},
    {id: 1, title: 'เงินสด / Cash'},
    {id: 2, title: 'เงินโอน / Transfer'},
    {id: 3, title: 'เช็ค / Check'}],
  masterbank: [
    {
      'title': 'Select Bank',
      'description': ''
    },
    {
      'title': 'กรุงเทพ',
      'description': 'Bangkok Bank'
    },
    {
      'title': 'กรุงศรีอยุธยา',
      'description': 'Bank of Ayudhaya'
    },
    {
      'title': 'กสิกรไทย',
      'description': 'KasikornBank'
    },
    {
      'title': 'เกียรตินาคิน',
      'description': 'Kiatnakin Bank'
    },
    {
      'title': 'ไทย',
      'description': 'BankThai'
    },
    {
      'title': 'ไทยพาณิชย์',
      'description': 'Siam Commercial Bank'
    },
    {
      'title': 'ธนชาต',
      'description': 'Thanachart Bank'
    },
    {
      'title': 'นครหลวงไทย',
      'description': 'Siam City Bank'
    },
    {
      'title': 'ยูโอบี',
      'description': 'United Overseas Bank, Thailand'
    },
    {
      'title': 'สแตนดาร์ดชาร์เตอร์ด',
      'description': 'Standard Chartered Bank Thai'
    },
    {
      'title': 'เมกะสากลพาณิชย์',
      'description': 'Mega International Commercial Bank'
    },
    {
      'title': 'สินเอเชีย',
      'description': 'Asia Credit Limited Bank'
    },
    {
      'title': 'เอสเอ็มอี',
      'description': '(SME) SME Bank of Thailand'
    },
    {
      'title': 'ธกส.',
      'description': 'Bank for Agriculture and Agricultural Cooperatives'
    },
    {
      'title': 'เพื่อการส่งออกและนำเข้า',
      'description': 'Export-Import Bank of Thailand'
    },
    {
      'title': 'อาคารสงเคราะห์',
      'description': 'Government Housing Bank'
    },
    {
      'title': 'อิสลามแห่งประเทศไทย',
      'description': 'slamic Bank of Thailand'
    },
    {
      'title': 'กรุงไทย',
      'description': 'Krung Thai Bank'
    },
    {
      'title': 'ซิติแบงก์',
      'description': 'Citibank'
    },
    {
      'title': 'ทหารไทย',
      'description': 'Thai Military Bank'
    },
    {
      'title': 'ทิสโก้',
      'description': 'Thai Investment and Securities Company Bank'
    },
    {
      'title': 'ออมสิน',
      'description': 'Government Saving Bank'
    }],
  activities: [
    {id: 0, title: 'เข้าพบ', name: 'visit', show: true},
    {id: 1, title: 'เก็บเงิน', name: 'visitcollection', show: true},
    {id: 2, title: 'วางบิล', name: 'visitbilling', show: true},
    {id: 3, title: 'เปิดออร์เดอร์', name: 'visitorders', show: false},
    {id: 4,
      title: 'กิจกรรม',
      name: 'visitactivities',
      show: true,
      detail: [
        {id: 1, title: 'ตกแต่งหน้าร้าน', status: false},
        {id: 2, title: 'แจกของ Premium', status: false},
        {id: 3, title: 'Update สินค้าใหม่', status: false},
        {id: 4, title: 'แจกโปรชัวร์', status: false}
      ]
    },
    {id: 5, title: 'บันทึกสภาพการตลาด', name: 'visitconditions', show: true},
    {id: 6, title: 'บันทึกคู่แข่ง', name: 'visitcompetitors', show: false},
    {id: 7, title: 'บันทึกลูกค้าค้าดหวัง', name: 'visitpostpect', show: false}
  ],
  filterorder: [
    {id: 0, title: 'สถานะยังไม่อนุมัติ', value: 1, name: 'approve', filter: 'asc'},
    {id: 1, title: 'วันที่เปิดออร์เดอร์ล่าสุด', value: 2, name: 'updateat', filter: 'desc'}
  ],
  filterappointment: [
    {id: 0, title: 'รูปแบบการกรองข้อมูล', value: 0, name: 'territory', filter: 'asc'},
    {id: 1, title: 'ยังไม่ว่างแผน', value: 1, name: 'status', filter: 'asc'},
    {id: 2, title: 'รออนุมัติ', value: 2, name: 'status', filter: 'desc'},
    {id: 3, title: 'วันที่เข้าพบล่าสุด', value: 3, name: 'scheduedate', filter: 'desc'}
    // {id: 4, title: 'ไม่อนุมัติ', value: 4, name: 'updateat', filter: 'desc'}
  ],
  activitiesvisit: [
    {id: 1, title: 'ตกแต่งหน้าร้าน', status: true},
    {id: 2, title: 'แจกของ Premium', status: true},
    {id: 3, title: 'Update สินค้าใหม่', status: true},
    {id: 4, title: 'แจกโปรชัวร์', status: true}
  ],
  documentsnewaccount: [
    {id: 1, title: 'ภพ.20_หนังสือรับรอง_เอกสารจดทะเบียนพานิช', status: true},
    {id: 2, title: 'สำเนาทะเบียนบ้าน', status: true},
    {id: 3, title: 'สำเนาบัตรประจำตัวประชาชน', status: true},
    {id: 4, title: 'เอกสารอื่นๆ_ป้ายหน้าร้าน', status: true}
  ],
  dropboxkey: 'EzaPxEj8x2AAAAAAAAA2kyELVweAKCBfQvfY0cxCGx3mb_cTmtr4ktM7W3M3TAtt',
  auth: 'Passw0rd@1',
  territory: null
}
