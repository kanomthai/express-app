const r = require('rethinkdb')
export default {
  testIns: () => {
    return new Promise(resolve => {
      r.connect({ host: 'localhost', port: 28015 }, function (err, conn) {
        if (err) throw err
        r.db('test').tableCreate('tv_shows').run(conn, function (err, res) {
          if (err) throw err
          console.log(res)
          r.table('tv_shows').insert({ name: 'Star Trek TNG' }).run(conn, function (err, res) {
            if (err) throw err
            console.log(res)
            resolve(res)
          })
        })
      })
    })
  }
}

// apiKey: 'AIzaSyDYVQJo4EnGpJGFkxigS8ruC9nMygGDBd4',
// authDomain: 'sales-ef617.firebaseapp.com',
// databaseURL: 'https://sales-ef617.firebaseio.com',
// projectId: 'sales-ef617',
// storageBucket: 'sales-ef617.appspot.com',
// messagingSenderId: '949588289'
