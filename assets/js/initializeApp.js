import Vue from 'vue'
import firebase from 'firebase'
import VueFire from 'vuefire'
import { firebaseConfig } from './config'
export const InitFireBase = () => {
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  } else {
    firebase.app()
  }
  Vue.use(VueFire)
}
