import Dropbox from 'dropbox'
import mock from './mock'
import r from 'rethinkdb'
import $ from 'jquery'
let guuid = require('./greeter')
function getFile (file) {
  return new Promise(resolve => {
    if (file) {
      var reader = new window.FileReader()
      reader.readAsDataURL(file)
      reader.onload = e => {
        resolve(e.target.result)
      }
    }
  })
}

function getLinkDropbox (txt) {
  return new Promise(resolve => {
    let token = mock.dropboxkey
    var settings = {
      async: true,
      crossDomain: true,
      url: 'https://api.dropboxapi.com/2/sharing/create_shared_link',
      method: 'POST',
      headers: {
        'authorization': 'Bearer ' + token,
        'content-type': 'application/json'
      },
      data: JSON.stringify({path: txt, short_url: false})
    }
    $.ajax(settings).done(data => {
      var u = data.url
      var link = 'https://dl.dropboxusercontent.com/' + u.substr(24, u.length)
      resolve(link)
    })
  })
}

function getResizeBlob (file) {
  return new Promise(resolve => {
    var reader = new window.FileReader()
    reader.onload = readerEvent => {
      var image = new window.Image()
      image.onload = imageEvent => {
        // Resize the image
        var canvas = document.createElement('canvas')
        // let max_size = 1200
        var width = image.width
        var height = image.height
        if (width > height) {
          if (width > 1200) {
            height *= 1200 / width
            width = 1200
          }
        } else {
          if (height > 1200) {
            width *= 1200 / height
            height = 1200
          }
        }
        canvas.width = width
        canvas.height = height
        canvas.getContext('2d').drawImage(image, 0, 0, width, height)
        canvas.toDataURL('image/png')
      }
      image.src = readerEvent.target.result
    }
    reader.readAsDataURL(file)
  })
}

export default {
  getFile: file => {
    return new Promise(resolve => {
      if (file) {
        var reader = new window.FileReader()
        reader.readAsDataURL(file)
        reader.onload = e => {
          resolve(e.target.result)
        }
      }
    })
  },
  loadBlob: async file => {
    var data = await getFile(file)
    return data
  },
  loadBlobResive: async file => {
    var data = await getResizeBlob(file)
    return data
  },
  GetCanvasAnnote: file => {
    return new Promise(resolve => {
      var reader = new window.FileReader()
      reader.onload = readerEvent => {
        var image = new window.Image()
        image.onload = imageEvent => {
          var canvas = document.createElement('canvas')
          // let max_size = 1200
          var width = image.width
          var height = image.height
          if (width > height) {
            if (width > 200) {
              // height *= 200 / width
              // width = 200
              height = 642
              width = 480
            }
          } else {
            if (height > 200) {
              // width *= 200 / height
              // height = 200
              height = 642
              width = 480
            }
          }
          canvas.width = width
          canvas.height = height
          canvas.getContext('2d').drawImage(image, 0, 0, width, height)
          canvas.toDataURL('image/png')
          var base64String = canvas.toDataURL()
          // console.dir(base64String)
          resolve(base64String)
        }
        image.src = readerEvent.target.result
      }
      reader.readAsDataURL(file)
    })
  },
  testIns: () => {
    return new Promise(resolve => {
      r.connect({ host: '127.0.0.1', port: 8080 }, function (err, conn) {
        if (err) throw err
        resolve(conn)
        // r.db('test').tableCreate('tv_shows').run(conn, function (err, res) {
        //   if (err) throw err
        //   console.log(res)
        //   r.table('tv_shows').insert({ name: 'Star Trek TNG' }).run(conn, function (err, res) {
        //     if (err) throw err
        //     console.log(res)
        //     resolve(res)
        //   })
        // })
      })
    })
  },
  imgDropboxImagesDelete: txt => {
    return new Promise(resolve => {
      let token = mock.dropboxkey
      var settings = {
        async: true,
        crossDomain: true,
        url: 'https://api.dropboxapi.com/2/files/delete_v2',
        method: 'POST',
        headers: {
          'authorization': 'Bearer ' + token,
          'content-type': 'application/json'
        },
        data: JSON.stringify({path: txt})
      }
      $.ajax(settings).done(data => {
        resolve()
      })
    })
  },
  imgDropboxImagesLink: txt => {
    return new Promise(resolve => {
      let token = mock.dropboxkey
      var settings = {
        async: true,
        crossDomain: true,
        url: 'https://api.dropboxapi.com/2/sharing/create_shared_link',
        method: 'POST',
        headers: {
          'authorization': 'Bearer ' + token,
          'content-type': 'application/json'
        },
        data: JSON.stringify({path: txt, short_url: false})
      }
      $.ajax(settings).done(data => {
        var u = data.url
        var link = 'https://dl.dropboxusercontent.com/' + u.substr(24, u.length)
        resolve(link)
      })
    })
  },
  doUpload: (event, txt, ptname) => {
    return new Promise(resolve => {
      console.dir(mock.dropboxkey)
      var ACCESS_TOKEN = mock.dropboxkey
      var dbx = new Dropbox.Dropbox({ accessToken: ACCESS_TOKEN })
      var fileInput = event.target
      var file = fileInput.files[0]
      let n = file.name
      var ft = n.substr((n.length - 3), n.length)
      if (ft === 'peg') {
        ft = 'jpeg'
      }
      let name = guuid.GenCode() + '.' + ft
      let dname = ptname + name
      dbx.filesUpload({path: dname, contents: file})
        .then((response) => {
          // console.dir(response)
          getLinkDropbox(dname)
            .then(rd => {
              resolve({
                msg: 'อัพโหลดรูปเสร็จแล้ว กดอีกครั้งเพื่อโหลดรูปใหม่',
                title: txt,
                pathname: dname,
                link: rd
              })
            })
        })
        .catch((error) => {
          console.error(error)
          resolve({
            msg: 'ไม่สามรถอัพโหลดรูปได้',
            title: null,
            pathname: null,
            link: null
          })
        })
    })
  }
}
