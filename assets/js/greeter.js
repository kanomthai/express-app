function guid () {
  function s4 () {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1)
  }
  return (s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4()).toUpperCase()
}
// var gkey = require('./cloud-yss')
module.exports = {
  GenCode: () => {
    var g = guid()
    return g.toUpperCase()
  },
  ReDate: (e) => {
    return e.substring(0, 10)
  },
  ReOrderLine: (e) => {
    var x = 0
    for (let i in e) {
      x = x + (((e[i].price * parseFloat(e[i].perc)) / 100) * e[i].qty)
    }
    return x.toLocaleString()
  },
  ReMasterOrderConst: () => {
    return 3000
  },
  Reavatar: () => {
    return '../../assets/img/avatar.svg'
  },
  GenCodeAppointment: (customer, empcode) => {
    var n = new Date()
    var m = '0' + (n.getDate() + 2)
    var y = n.getFullYear()
    if (m > 12) {
      m = '0' + 1
      y = n.getFullYear() + 1
    }
    return ('0' + n.getDate()).slice(-2) + '' + m.slice(-2) + '' + y + customer.substring(1, customer.length) + '' + empcode.substring(3, empcode.length)
  },
  getDropboxToken: () => {
    return 'EzaPxEj8x2AAAAAAAAA1P2gS-esVrO-yrd_r1yjO7nLpkm-Ey7hyYhsEcqUyTFZ'
  }
}
