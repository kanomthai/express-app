import axios from 'axios'

var checkToken = token => {
  return new Promise(resolve => {
    axios.get('/api/auth/verify', {
      headers: {
        'x-access-token': token
      }
    })
      .then(res => {
        if (res.data.data.err === true) {
          resolve({redirec: '/auth/login', data: res.data.data})
        } else {
          resolve({redirec: null, data: res.data.data})
        }
      })
      .catch(err => {
        resolve(err)
      })
  })
}
var checkTokenLeadder = (tokenid, token) => {
  return new Promise(resolve => {
    axios.post('/api/auth/verify/leadid', {
      tokenid: tokenid
    }, {
      headers: {
        'x-access-token': token
      }
    })
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        resolve(err)
      })
  })
}
var checkTerritoryId = (territoryid, token) => {
  return new Promise(resolve => {
    axios.post('/api/auth/verify/territoryid', {
      territoryid: territoryid
    }, {
      headers: {
        'x-access-token': token
      }
    })
      .then(res => {
        resolve(res)
      })
  })
}
var checkImages = (code, token) => {
  return new Promise(resolve => {
    axios.post('/api/auth/verify/images', {
      code: code
    }, {
      headers: {
        'x-access-token': token
      }
    })
      .then(res => {
        resolve(res)
      })
  })
}
var checkImagesRefid = (code, token) => {
  return new Promise(resolve => {
    axios.post('/api/auth/verify/images/refid', {
      code: code
    }, {
      headers: {
        'x-access-token': token
      }
    })
      .then(res => {
        resolve(res)
      })
  })
}
export default {
  checkToken: async token => {
    var data = await checkToken(token)
    console.dir('data token')
    console.dir(data)
    return data
  },
  checkTerritorylist: async (tokenid, token) => {
    var data = await checkTokenLeadder(tokenid, token)
    return data
  },
  getcheckTerritoryId: async (territoryid, token) => {
    var data = await checkTerritoryId(territoryid, token)
    return data
  },
  getImages: async (code, token) => {
    var data = await checkImages(code, token)
    return data
  },
  getImagesRefid: async (code, token) => {
    var data = await checkImagesRefid(code, token)
    return data
  }
}
