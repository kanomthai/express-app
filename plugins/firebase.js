import Vue from 'vue'
import VueFire from 'vuefire'
import firebase from 'firebase'
const firebaseConfig = {
  apiKey: 'AIzaSyCEgPnuWk5IQjVWNbrkUKNDiU6lZEYss10',
  authDomain: 'grounded-burner-199606.firebaseapp.com',
  databaseURL: 'https://grounded-burner-199606.firebaseio.com',
  projectId: 'grounded-burner-199606',
  storageBucket: 'grounded-burner-199606.appspot.com',
  messagingSenderId: '1072089269744'
}
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}
Vue.config.productionTip = false
Vue.use(VueFire)
window.db = firebase.database()
window.auth = firebase.auth()
window.firestore = firebase.firestore()
window.timesz = firebase.database.ServerValue.TIMESTAMP
