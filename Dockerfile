FROM node:8.11-alpine
ENV NODE_ENV production

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app
RUN npm install
RUN npm run build
COPY . /usr/src/app/

EXPOSE 3000
CMD ["npm","start"]
